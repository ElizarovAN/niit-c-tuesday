#include <stdio.h>
#include <string.h>


typedef struct SYM TSYM;
typedef TSYM *PSYM;
PSYM psym[256] = { 0 };


typedef struct SYM
{
    char symbol;
    float freq;
    char code[256]; //������ ��� ������ ����
    struct SYM *left; //����� �������
    struct SYM *right; //������ �������
}SYM;



struct SYM* buildTree(int N)
{
    // ������ ��������� ����
    PSYM temp = (struct SYM*)malloc(sizeof(struct SYM));
    // � ���� ������� ������������ ����� ������
    // ���������� � �������������� ��������� ������� psym
    temp->freq = psym[N-2]->freq + psym[N - 1]->freq;
    // ��������� ��������� ���� � ����� ���������� ������
    temp->left = psym[N - 1];
    temp->right = psym[N - 2];
    temp->code[0] = 0;
    if (N == 2)
        return temp;
    // ��������� temp � ������ ������� psym,
    // �������� ������� �������� �������
    int count = N - 1;
    for (int i = 0; i < N; i++)
    {
        if (psym[i]->freq < temp->freq)
        {
            if (count>2)
            {
                for (int a = count - 2; a >= i; a--)
                {
                    psym[a + 1] = psym[a];
                }
                psym[i] = temp;
                break;
            }
            else
            {
                if (count == 2)
                {
                    psym[i + 1] = psym[i];
                    psym[i] = temp;
                    break;
                }
            }
        }
        else
            continue;
     }     
      return buildTree(N - 1);
}

void makeCodes(PSYM root)
{
    
    if (root->left)
    {
        strcpy(root->left->code, root->code);
        strcat(root->left->code, "0");
        makeCodes(root->left);
    }
    if (root->right)
    {
        strcpy(root->right->code, root->code);
        strcat(root->right->code, "1");
        makeCodes(root->right);
    }
 
}

typedef union CODE
{
    unsigned char ch;
    struct 
    {
        unsigned short b1 : 1;
        unsigned short b2 : 1;
        unsigned short b3 : 1;
        unsigned short b4 : 1;
        unsigned short b5 : 1;
        unsigned short b6 : 1;
        unsigned short b7 : 1;
        unsigned short b8 : 1;
    }byte;
}CODE;

char pack(unsigned char *buf) 
{
    CODE code;
    code.byte.b1 = buf[0] - '0';
    code.byte.b2 = buf[1] - '0';
    code.byte.b3 = buf[2] - '0';
    code.byte.b4 = buf[3] - '0';
    code.byte.b5 = buf[4] - '0';
    code.byte.b6 = buf[5] - '0';
    code.byte.b7 = buf[6] - '0';
    code.byte.b8 = buf[7] - '0';
    return code.ch;
}

int tail_length(FILE *infile,PSYM sym,int i)
{
    rewind(infile);
    char mas[8] = { 0 };
    int c = 0,a=0,b=0;
    while ((c= fgetc(infile)) != -1 && c != '\n')
    {
        int count = 0;
        for (a = 0; a <= i; a++)
        {
            if (sym[a].symbol == (unsigned char)c)
            {
                for (count = 0; count < strlen(sym[a].code); count++)
                {
                    mas[b++] = sym[a].code[count];
                    if (b > 7)
                    {
                        for (b = 0; b < 8; b++)
                            mas[b] = 0;
                        b = 0;
                    }
                }
                break;
            }
        }
    }
    if(b<8)
        return b;
    else 
        return 0;
}

void print(TSYM *sym, int count_struct)
{
    for (int i = 0; i <= count_struct; i++)
    {
        printf("symbol %c repits %.3fpersent\n", sym[i].symbol,
            sym[i].freq);
    }
}

int cmp(int count_struct)
{
    int i = count_struct;
    int b = i - 1, a = count_struct;
    PSYM temp = 0;
    for (i, b; i >= 1, b >= 0; i--, b--)
    {
        if (psym[i] != NULL)
        {
            if (psym[i]->freq > psym[b]->freq)
            {
                temp = psym[b];
                psym[b] = psym[i];
                psym[i] = temp;
                a = b;
                for (a; a< count_struct - 1; a++)
                {
                    if (psym[a]->freq < psym[a + 1]->freq)
                    {
                        temp = psym[a];
                        psym[a] = psym[a + 1];
                        psym[a + 1] = temp;
                    }
                    else
                        continue;
                    a++;
                }
            }
            else
                continue;
        }
        else
            continue;
    }
}


