/*
�������� ���������, ������� ��� �������� ������ ���������� ��-
�������� ���� � ������� ������ ����� �� ��������� ������ � ���
�����
���������:
����� ����������� ����� ����������� ��������.
*/

//By Anoshkin D.M.

#include <stdio.h>
#include <string.h>

#define MaxLen 80
int main()
{
    char str[MaxLen];
    char inWord=0;
    char i=0;
    unsigned char count=0;

    puts("Enter a line:");
    fgets(str, MaxLen, stdin);
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=' ';

    while (str[i])
    {
        if(str[i]!=' ')                 
        {
            if (inWord==0)
                inWord=1; 
            count++;
            putchar (str[i]);
        }
        else if(str[i]==' ' && inWord==1)
        {
            printf (" - %u \n", count);
            count=0;
            inWord=0;
        }
        i++;
    }
    if (inWord==1)
    {
        printf (" - %u \n", count);
        count=0;
        inWord=0;
    }
    return 0;
}
