/*
�������� ���������, �������������� ��������� ������� �����-
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
���������:
1
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ����������� �������� �� ����� � ����������� ����-
����� ������� �����.
*/

// by Anoshkin Dmitry

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

#define MaxLen 80

int getWords (char*strs, char**pstn, int len)
{
    int count=0;
    int i=0;
    while(*(strs+i)!=10 && *(strs+i)!=0)
    {
        while(*(strs+i)==' ') i++;
        if (*(strs+i)!=' '&& *(strs+i)!=10 && *(strs+i)!=0)
        {
            pstn[count]=strs+i;
            count++;
            while(*(strs+i)!=' '&& *(strs+i)!=10 && *(strs+i)!=0) i++;
        }
    }
    return count;
}


void WordRndChange(char * pstn)
{
    int i=0, j, rnd;
    char tmp=0;
    while(*(pstn+i)!=' '&& *(pstn+i)!=10 && *(pstn+i)!=0)
        i++;
    if (i>3)
        for (j=1;j<i-2;j++)
        {
            tmp=*(pstn+j);
            rnd=rand()%(i-j-1);
            *(pstn+j)=*(pstn+j+rnd);
            *(pstn+j+rnd)=tmp;
        }
}

int main()
{
    FILE *in;                               // ��������� �� ����
    char strs[MaxLen]={0};                  
    char *pstn[MaxLen/2]={strs};
    int count=0;
    int i;
    srand(time(NULL));
    in=fopen("in.txt", "rt");
    while (fgets(strs, MaxLen, in)!=0)
    {
        count=getWords (strs, pstn, MaxLen/2);
        for (i=0; i<count; i++)
            WordRndChange (pstn[i]);

        printf("%s\n", strs);
    }
    return 0;
}