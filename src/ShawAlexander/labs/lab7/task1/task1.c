/*1. �������� ���������, ��������� ��������� ������ � �������� �
�������� � �� ����� � ������������ � ����������� ����� ������
��������� ������ ������������ ��������� ������� :
(a)������������ ������ �� ������ ������ �����.
(b)����� � ����� ���� ������ �� ���������� ����������� ������.
(c)����� ����������� ������� �� ��������*/
#include <stdio.h>
#include <String.h>
typedef struct ITEM {
    char *region;
    char *country;
    int code;
    struct ITEM *next;
} TITEM;
typedef TITEM * PITEM;
PITEM _head = NULL, _tail = NULL;
void getItem();
void fillItem(char*);
PITEM getItemByRegion();
void printCountryInfo(char*);
int main(int argc, char *argv[]) {
    if (argc < 2) {
        puts("Usage: program_name file_path");
        exit(1);
    }
    FILE *fp = fopen(argv[1], "rt");
    char buf[128] = { 0 };
    fgets(buf, 128, fp);
    while (fgets(buf, 128, fp)) {
        buf[strlen(buf) - 1] = 0;
        getItem();
        fillItem(buf);
    }
    printCountryInfo("US");
    getItemByRegion("Moskva");
    return 0;
}
//���������� �������� � ������
void getItem() {
    PITEM item = (PITEM)malloc(sizeof(TITEM));
    if (!_head)
        _head = item;
    if (!_tail)
        _tail = item;
    item->region = (char*)malloc(sizeof(char) * 128);
    item->country = (char*)malloc(sizeof(char) * 128);
    item->next = NULL;
    item->code = 0;
    _tail->next = item;
    _tail = item;
}
//����� � ����� ���� ������ �� ���������� ����������� ������
void printCountryInfo(char *country) {
    PITEM h = _head;
    printf("Country: %s\n*****\n", country);
    while (h) {
        if (!strcmp(country, h->country)) {
            printf("Region: %s (%02d)\n", h->region, h->code);
        }
        h = h->next;
    }
    puts("*****");
}
//����� ����������� ������� �� ��������
PITEM getItemByRegion(char *rgn) {
    char *region = (char*)malloc(sizeof(rgn) + 3);
    memset(region, 0, strlen(region));
    region[0] = '\"';
    strcat(region, rgn);
    region[strlen(region)] = '\"';
    PITEM h = _head;
    while (h) {
        if (!strcmp(region, h->region)) {
            printf("Region: %s\nCountry: %s\nCode: %02d\n", h->region, h->country, h->code);
            return h;
        }
        h = h->next;
    }
    printf("ERROR: Region not found\n");
    return NULL;
}
//���������� ������ ���������� �� ������
void fillItem(char *line) {
    line = strtok(line, ",");
    strcpy(_tail->country, line);
    line = strtok(NULL, ",");
    _tail->code = atoi(line);
    line = strtok(NULL, ",");
    strcpy(_tail->region, line);
}