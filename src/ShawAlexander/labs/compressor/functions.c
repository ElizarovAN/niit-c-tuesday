#include "functions.h"

int getUniques(FILE *fp) {
    unsigned char chars[256] = { 0 };
    int c = 0;
    int uniques = 0;
    rewind(fp);
    while ((c = fgetc(fp)) != EOF) {
        if (uniques == 256)
            break;
        if (!chars[c]) {
            chars[c] = 1;
            uniques++;
        }
    }
    return uniques;
}
void resetSyms(SYM *syms, unsigned int size) {
    for (unsigned int i = 0; i < size; i++) {
        syms[i].left = syms[i].right = NULL;
        syms[i].ch = 0;
        syms[i].freq = 0.0f;
        memset(syms[i].code, 0, 256);
    }
}
void fillpArr(SYM **pointers, SYM *arr, unsigned int size) {
    for (unsigned int i = 0; i < size; i++) {
        if (!arr[i].freq)
            break;
        pointers[i] = &(arr[i]);
    }
}
int buildFreqTable(SYM *syms, unsigned int size, FILE *fp) {
    int c = 0;
    unsigned int total = 0;
    rewind(fp);
    while ((c = fgetc(fp)) != EOF) {
        syms[c].ch = (unsigned char) c;
        syms[c].freq += 1.0f;
        total++;
    }
    for (unsigned int i = 0; i < size; i++) {
        syms[i].freq /= total;
    }
    return total;
}
void sortSyms(SYM *syms, int size) {
    for (int i = size - 1; i > 0; i--) {
        for (int q = 0; q < i; q++) {
            if (syms[q].freq < syms[q + 1].freq) {
                SYM tmp = syms[q];
                syms[q] = syms[q + 1];
                syms[q + 1] = tmp;
            }
        }
    }
}
SYM* buildTree(SYM **psyms, unsigned int N) {
    SYM *temp = (SYM*)malloc(sizeof(SYM));
    //freq contains the sum of the last element and the penultimate element
    temp->freq = psyms[N - 1]->freq + psyms[N - 2]->freq;
    //Linking created node with two last elements
    temp->left = psyms[N - 1];
    temp->right = psyms[N - 2];
    temp->code[0] = 0;
    if (N == 2) //root node with 1.0 frequency created
        return temp;
    //adding temp on the right position to psyms
    for (unsigned int i = 0; i < N; i++) {
        if (psyms[i]->freq < temp->freq) {
            if (i + 1 == N - 1) {
                psyms[i] = temp;
                break;
            }
            else if (i + 2 == N - 1) {
                psyms[i + 1] = psyms[i];
                psyms[i] = temp;
                break;
            }
            else {
                for (int q = N - 3; q >= 0 && q >= i; q--) {
                    psyms[q + 1] = psyms[q];
                }
                psyms[i] = temp;
                break;
            }
        }
    }
    return buildTree(psyms, N - 1);
}
void makeCodes(SYM *root) {
    if (root->left) {
        strcpy(root->left->code, root->code);
        strcat(root->left->code, "0");
        makeCodes(root->left);
    }
    if (root->right) {
        strcpy(root->right->code, root->code);
        strcat(root->right->code, "1");
        makeCodes(root->right);
    }
}
char pack(unsigned char *arr) {
    CODE code;
    code.byte.b1 = arr[0] - '0';
    code.byte.b2 = arr[1] - '0';
    code.byte.b3 = arr[2] - '0';
    code.byte.b4 = arr[3] - '0';
    code.byte.b5 = arr[4] - '0';
    code.byte.b6 = arr[5] - '0';
    code.byte.b7 = arr[6] - '0';
    code.byte.b8 = arr[7] - '0';
    return code.ch;
}