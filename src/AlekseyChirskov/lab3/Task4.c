/*�������� ���������, ������� ������� ����� ����� �� �������� ������
���������:
��������� ������������� ����������� ������������������ ���� � ������ ���
����� � ������������ �� ��� ������ �����. � ��������� ������������� ������-
����� �� ������������ ����� ��������, �� ���� ���� ������������ ������ �����
������� ������������������ ����, � ����� ������� �� ��������� �����.*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define M 5//����������� �� ������������ ����� ��������
#define N 80//����� ������
int fun(char *garbage,int size)//������� fun ��������� ����� �� ��������� ����� ����
{							   //� ���������� ��, ���� ����� �������� ������ M
	char result[N]={0};
	int i,j,n=0,summ=0,m,d;
	d=strlen(garbage)/M;
	m=strlen(garbage)/M;
	while(d)
	{
		i=0;
		memset(result,0,sizeof(result));
		for(j=n;j<M+n;j++)
		{
			result[i]=garbage[j];
			i++;
		}
		summ+=atoi(result);//result ������ ��� ��������� ������� garbage �� ��������� ����� ����
		d--;
		n+=M;
	}
	n=0;
	if(strlen(garbage)%M!=0)
	{
		memset(result,0,sizeof(result));
		i=0;
		for(j=M*m;j<M*m+(strlen(garbage)%M);j++)
		{
			result[i]=garbage[j];
			i++;
		}
		summ+=atoi(result);
	}	
	return summ;
}
int main() 
{
	char c,str[N],garbage[N]={0};//garbage ������ � ������� ������
	int i,j=0,n=0,summ=0,size;
	puts("Enter the line: ");
	fgets(str,N,stdin);
	for(i=0;i<strlen(str);i++)
    {
		if(str[i]>='0' && str[i]<='9')
		{
			garbage[j]=str[i];
			j++;
			n=1;
		}
		else n=0;
		if(n==1)
			continue;
		if(n==0)
		{
			if(strlen(garbage)>M)
			{
				size=strlen(garbage);
				summ+=fun(garbage,size);
			}
			else summ+=atoi(garbage);
			memset(garbage,0,sizeof(garbage));
			j=0;
		}
	}
	if(strlen(garbage)>M)
	{
		size=strlen(garbage);
		summ+=fun(garbage,size);
	}
	else summ+=atoi(garbage);
	printf("%d\n",summ);
	return 0;
}