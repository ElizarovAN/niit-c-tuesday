#include "compressor.h"

void freq(FILE *fp, PSYM syms)
{
	int c=0;
	while((c=fgetc(fp))!=EOF)
    {
		{
			syms[c].ch=c;
			syms[c].freq++;
		}
    }
	qsort(syms,M,sizeof(TSYM),comp);
}
int uniqueSYMcount(PSYM syms)
{
	int count=0;
	while(syms[count].freq!=0)
        count++;
	return count;
}
PSYM buildTree(PSYM *psym, int N)
{
	int i,j;
	PSYM temp=(PSYM)malloc(sizeof(TSYM));
	temp->freq=psym[N-2]->freq+psym[N-1]->freq;
	temp->left=psym[N-1];
	temp->right=psym[N-2];
	temp->code[0]=0;
	if(N==2)
	return temp;
	else{
		for(i=0;i<N;i++)
			if(temp->freq>psym[i]->freq)
			{   
				for(j=N-1;j>i;j--)
					psym[j]=psym[j-1];
				psym[i]=temp;
				break;
			}       
	}
	return buildTree(psym,N-1);
}
void makeCodes(PSYM node)
{
	if(node->left)
	{
		strcpy(node->left->code,node->code);
		strcat(node->left->code,"0");
		makeCodes(node->left);
	}
	if(node->right)
	{
		strcpy(node->right->code,node->code);
		strcat(node->right->code,"1");
		makeCodes(node->right);
	}
}
int comp(const void *a, const void *b)
{
	return (*(PSYM)b).freq - (*(PSYM)a).freq;
}
UC pack(UC *buf)
{
	union CODE code;
	code.byte.b1=buf[0]-'0';
	code.byte.b2=buf[1]-'0';
	code.byte.b3=buf[2]-'0';
	code.byte.b4=buf[3]-'0';
	code.byte.b5=buf[4]-'0';
	code.byte.b6=buf[5]-'0';
	code.byte.b7=buf[6]-'0';
	code.byte.b8=buf[7]-'0';
	return code.ch;
}
long getFileLength(FILE *file)
{
	long length;
	rewind(file);
	fseek(file,0,SEEK_END);
	length=ftell(file);
	fclose(file);
	return length;
}
void swap(char *pstr, int one, int two)
{
	char temp=pstr[one];
	pstr[one]=pstr[two];
	pstr[two]=temp;
}
void coup(char *pstr, int i)
{
	int j;
	for(j=0;j<=i/2;j++)
		swap(pstr,j,i-j);
}
int fileExtension(char **argv, char *pend)
{
	int i=0,length;
	length=strlen(argv[1]);
	while(argv[1][length-1-i]!='.')
	{
		pend[i]=argv[1][length-1-i];
		i++;
	}
	coup(pend,strlen(pend)-1);
	return i;
}
void  fileName(int *number, char **argv, char *pname)
{
	int i=0,length;
	length=strlen(argv[1]);
	while(argv[1][length-1-i]!=0)
	{
		pname[i]=argv[1][length-*number-2-i];
		i++;
	}
	coup(pname,strlen(pname)-1);
}