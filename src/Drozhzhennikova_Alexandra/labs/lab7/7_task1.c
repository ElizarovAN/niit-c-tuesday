#include<string.h>
#include<stdlib.h>
#include "7_task1.h"
extern PITEM head_1;

PITEM createList(PNAME_REC name_rec)//�������� ������
{
    PITEM item = (PITEM)malloc(sizeof(TITEM));// ��������� ��������� �����, ������������ ��������� �����
    item->name_rec = name_rec;
    item->prev = NULL;
    item->next = NULL;
    return item;
}

PNAME_REC createName(char *line)//������� ��������� ������  � ��������� ���� ������ � ������
{
    int i = 0;
    PNAME_REC rec = (PNAME_REC)malloc(sizeof(TNAME_REC));

    while (*line&&*line != '"'&&*line != ',')// ����� �������� � ������ � �������� ������� ����� ""
    {
        rec->abbr[i++] = *line++;
    }
    rec->abbr[i] = 0;
    i = 0;
    if (*line == ',')
    {
        line++;
        while (*line&&*line != '"'&&*line != ',')// ����� �������� � ������ � �������� ������� ����� ""
        {
            rec->code[i++] = *line++;
        }
        rec->code[i] = 0;
        i = 0;
    }
    if (*line == ',')
        {
             line++;
             if(*line == '"')
             {
                 line++;
              while (*line&&*line != '"')
              {
                 rec->name[i++] = *line++;
              }              
        }
        rec->name[i] = 0;
    }
    
    line++;//����������  � �������
    i = 0;
    return rec;
}
PITEM addToTail(PITEM tail,   // ���
    PNAME_REC name_rec)//���������, �������� � ����� ������
{
    PITEM item = createList(name_rec);
    if (tail != NULL)//��������� ����� ������
    {
        tail->next = item;
        item->prev = tail;//����� ��������� ������� ��������� �� ����������
    }
    return item;
}

int countList(PITEM head)// ������� �� � ������
{
    int count = 0;
    while (head)
    {
        count++;
        head = head->next;
    }
    return count;
}
PITEM findByName(PITEM head, char *name)//������ ����� ����� �� �����
{
    while (head)
    {
        if (strcmp(head->name_rec->name, name) == 0)
            return head;        
        head = head->next;
    }
    return NULL;
}
void printName(PITEM item)//������ �� �����
{
    if (item != NULL)
    {
        puts(item->name_rec->name);
        puts(item->name_rec->abbr);
        puts(item->name_rec->code);
    }
}
PITEM findByAbbr(PITEM head_1, char *abbr)//������ ����� ����� �� ����
{
    while (head_1)
    {
        if (strcmp(head_1->name_rec->abbr, abbr) == 0)
        {
            return head_1;
        }
    }
    return NULL;
}
void printAbbr(PITEM item)//������ �� ����
{
    if (item != NULL)
    {
        puts(item->name_rec->abbr);
        puts(item->name_rec->code);
        puts(item->name_rec->name);
    }
    return item;
}


