/*
�������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� ������ ��
�����. � ������ ������������� n ��������� ��������� �� ������
*/

//by Anoshkin Dmitry

#include <stdio.h>

#define MaxLen 100

int main()
{
    char str[MaxLen]={0};
    char i=0, wNum=0, InWord=0;
    int n=0;

    puts ("Input your string:");
    fgets (str, MaxLen, stdin);
    puts ("Input word's number:");
    if (scanf("%d", &n)<1 || n<=0)
        puts ("Input Error");
    else
        while (str[i]!=10 && str[i] !=0)
        {
            if(str[i]!=' ' && InWord==0)
            {
                wNum++;
                if (wNum==n)
                {
                    while (str[i]!=' ' && str[i]!=0 && str[i]!=10)
                        putchar (str[i++]);
                    return 0;
                }
                InWord=1;
            }
            else if (str[i]==' ') 
                InWord=0;
            i++;
        }
        puts ("The number is too big!");
    return 0;
}


