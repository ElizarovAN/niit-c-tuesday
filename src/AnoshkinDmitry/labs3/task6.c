/*
�������� ���������, ������� ��������� ������������� ������ ������� N,
� ����� ������� ����� ��������� ����� ����������� � ������������ ���-
�������.*/

//By Anoshkin D.M.

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define N 10
#define dev 10

int main()
{
    int arr[N];
    int i, sum=0, j=N-1, End=0;
    int min=0, max=0, indMax=0, indMin=0;
    srand(time(0));
    for (i=0; i<N; i++)
	{
		arr[i]=rand()%(dev*2)-dev;
        printf(" %d", arr[i]);
        if (i==0) 
        {
            min=arr[i];
            max=arr[i];
        }
        if (arr[i]<min)
        {
            min=arr[i];
            indMin=i;
        }
        if (arr[i]>max)
        {
            max=arr[i];
            indMax=i;
        }
    }
    puts ("\n");
    printf ("min = %d max = %d \n", min, max); 
    if (indMax>indMin)
    {
        i=indMin;
        End=indMax;
    }
    else
    {
        i=indMax;
        End=indMin;
    }
    i++;
    for (;i<End;i++) 
             sum=sum+arr[i];
    printf ("Sum = %d \n", sum);
    return 0;
}
