/*
�������� ���������, ������� ��������� ������������� ������ ������� N, �
����� ������� ����� ���������, ������������� ����� ������ ����������-
��� � ��������� ������������� ����������.
���������:
������ ����������� ���������� �������: �������������� � ������������-
�� ������� (��� ����� �������...)
*/

//By Anoshkin D.M.

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define N 10
#define dev 10

int main()
{
    int arr[N];
    int i, sum=0, j=N-1, tmp=0;
    srand(time(0));
    for (i=0; i<N; i++)
	{
		arr[i]=rand()%(dev*2)-dev;
        printf(" %d", arr[i]);
    }
    puts ("\n");
    i=0;
    while(arr[i]>=0 && i<N) i++;      //������� ����� ����� �������������
    while(arr[j]<=0 && j>=0) j--;     //������� ����� ������ �������������, ������ ��������������
    i++;
    for (;i<j;i++) 
             sum=sum+arr[i];
    printf ("Sum = %d \n", sum);
    return 0;
}
