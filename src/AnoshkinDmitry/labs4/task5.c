/*
�������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.
*/
// by Anoshkin Dmitry

#include <stdio.h>
#include <string.h>

#define MaxCount 20 //max count of strings
#define MaxLen 80 //max length of strings

int main()
{
    FILE *in, *out;       // ��������� �� ����
    char strs[MaxCount][MaxLen]={0};        //strings array
    char *str[MaxCount]={0};                //pointers for strings
    int i=0, j=0, count=0;
    char *tmp=0;

    out=fopen("out.txt", "wt");
    in=fopen("in.txt", "rt"); 
    if(in==0 || out==0) //error opening (0/1)
    {
        perror("File");    //������� ������
        return 1;
    }
    
    while (fgets(&strs[i], MaxLen, in)!=0 && i<MaxCount)
    {
        str[i]=strs[i];                    //����������� ���������� ������
        i++;
    }
    count=i-1;

    for (i=0; i<count-1; i++)               //���������� ���� ����������
    {
        for (j=i+1;j<count;j++)
            if (strlen(str[j])>strlen(str[i]))  //���������� ����� �����
            {
                tmp=str[j];                 //������ �������� ����������
                str[j]=str[i];
                str[i]=tmp;
            }
    }
    for (i=0; i<count; i++)
        fprintf(out, "%s", str[i]);
    
    fclose(in);
    fclose(out);
    return 0;
}