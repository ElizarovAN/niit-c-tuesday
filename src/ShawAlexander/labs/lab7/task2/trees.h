#include <stdio.h>
#include <string.h>
#include <stdlib.h>
typedef struct NODE {
    char * keyword;
    unsigned int timesUsed;
    struct NODE * left;
    struct NODE * right;
} TNODE;
typedef TNODE * PNODE;

//�������� ������� ����� ������
void chomp(char *string);
//��������� ������ ����
PNODE getNewNode();
//�������� ������ �������� ����
PNODE makeTree(FILE *keyFile);
//������� �������� � ������
PNODE insertWord(PNODE root, char *kword);
//������� ��������� � ������ � ������� ���������� ��������������
void insertInOrder(PNODE root, char **pwords, int pow, int elem);
//����� � ������ ���������� ��������
PNODE searchTree(PNODE root, char *string);
//������ ������ �������� ���� � �� ����������
void printTree(PNODE root);