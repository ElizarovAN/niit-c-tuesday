/*6. �������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������:
����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������.*/
#include <stdio.h>
#include <string.h>
#define N 80
#define M 150
void clean_stdin()
{
	int c;
	do {
		c=getchar();
	}while(c!='\n' && c !=EOF);
}
int main()
{
	char str[N][M];
	char *young=0,*old=0;
	int i=0,count,age,flag,age_old=0,age_young=M;
	do{
		flag=1;
		puts("Enter number of your family members: ");
		if(scanf("%d",&count)<1)
		{
			flag=0;
			puts("Error, please try again!\n");
			clean_stdin();
		}
	}

	while(flag==0);
	for(i=0;i<count;i++)
	{
		puts("Enter the name of your family member: ");
		clean_stdin();
		fgets(str[i],M,stdin);
		do{
			flag=1;
			puts("Enter the age: ");
			if(scanf("%d",&age)<1)
			{
				flag=0;
				puts("Error, please try again!\n");
			}
		}
		while(flag==0);
		if(age_old<=age)
		{
			age_old=age;
			old=str[i];
		}
		if(age_young>=age)
		{
			age_young=age;
			young=str[i];
		}
	}
	printf("The youngest member is %s\nThe oldest member is %s\n",young,old);
	return 0;
}