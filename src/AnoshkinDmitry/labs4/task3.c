/*
 �������� ���������, ������� ����������� ������ � ����������, �� ��������
�� ������ ����������� (��������� �������� � ����� ������� � ������ ������)
���������:
���� ������ - ��������� ��������� ��� �������� ������������ ������ � ����
������
*/

#include <stdio.h>
#include <string.h>

#define MaxLen 100

int main()
{
    char str[MaxLen]={0};
    char *c=str;
    int len=0;
    int i=0;

    puts("Input your string: ");
    fgets (str, MaxLen, stdin);
    len=strlen(str)-1;
    if (len>1)
    {
        while (i<len/2 &&
            *(c+i)==*(c+len-i-1))
            i++;
        if (i+1>len/2)
            puts ("String is palindrom");
        else
            puts ("String is not polindrom");
    }
    else
        puts ("String too short");
    return 0;
}
