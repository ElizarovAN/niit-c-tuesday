// ��������� �� ��������
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	char stars[100] = { 0 }, format[100] = { 0 };
	int num_raws = 0, index = 0, i = 0, j = 0, y = 1;
	
	while (1)
	{
		puts("Enter the number of raws: \n");
		scanf("%d", &num_raws);
		if (num_raws < 1 || num_raws > 100)
		{
			puts("Input error!\n");
			clean_stdin();
		}
		else break;
	}
	
	index = 1 + ((num_raws - 1) * 2);
	do {
		sprintf(format, "%c%d%c", '%', (index+j-num_raws+1), 's');
		do {
			stars[i] = '*';
			i++;
		} while (i < y);
		printf(format, stars);
		putchar('\n');
		j++;
		y+=2;
	} while (j < num_raws);
	putchar('\n');
	return 0;
}