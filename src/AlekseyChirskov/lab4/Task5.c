/*�������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 150
#define M 1024
int comp(const void *a, const void *b)
{
	return strlen(*(char**)a) - strlen(*(char**)b);
}
int main()
{
	FILE *fp,*out;
	char str[N][M];
	char *pstr[N]={0};
	int i,count=0;
	fp=fopen("input.txt","rt");
	out=fopen("result.txt","wt");
	if(fp==0)
	{
		perror("File:");
		return 1;
	}
	while (fgets(str[count],M,fp))
	{
		pstr[count]=str[count];
		count++;
	}
	qsort(pstr,count,sizeof(char*),comp);
	for(i=0;i<count;i++)
		fprintf(out,"%s",pstr[i]);
	fclose(fp);
	fclose(out);
	return 0;
}
