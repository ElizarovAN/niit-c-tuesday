/*
�������� ���������, ������� ������� ����� �� ���������
���������:
(a) �������� ������� � ���� ���������� ����������� �������;
2
(b) ��������� ������� - � ������;
(c) ��������� ���������� (�� �������), ��� ��������� ������ ������� ����-
��;
(d) ���� ������ ��������, ��������� ������������ � ������ ����� � ��
�����������;
(e) ��������� ����� � ������ �� ��������� ��� ����������� ��� �������
�������.
���������:
�������� �#� ���������� ����� ���������
�������� ������ - ��������� ������
�������� �X� - �������������� ��������
############################
#           #   #          #
##########  #   #          #
#           #   #######  ###
# ######    # x            #
#      #    #   #######   ##
#####  #    #   #         ##
       #        #     ######
############################
*/

// by Anoshkin Dmitry

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define MaxSize 256

int labirint(FILE *source, char ***Lab);
int getOut(char **Lab, int x, int y, int count); 
int main(int argc, char *argv[])
{
    FILE *source;
    char **Lab;
    int count, x, y;
    int i, j, check=0;

    if (argc!=2){
        puts("Input Error");
        return 1;
    }
    source=fopen(*(argv+1), "rt");
    if (source==NULL){
        printf("can't open file %s \n", *(argv+1));
        return 2;
    }
    count=labirint(source, &Lab);
    for (i=0; i<count; i++)
        for(j=0;j<strlen(Lab[i]);j++)
            if(Lab[i][j]=='x'){
                x=j;
                y=i;
                check=1;
            }
    if(check==0){
        puts("Error! No Hero!");
        return 3;
    }

    if (getOut(Lab, x, y, count))
        puts ("Exit found!");
    else
        puts ("No Exit!");

    return 0;
}

int labirint(FILE *source, char ***Lab)
{
    int count=0;
    int i;
    char Buf[MaxSize];

    while (fgets(Buf, MaxSize, source))
        count++;
    rewind (source);

    *Lab=(char**)calloc(count, sizeof(char*));
    for (i=0; i<count; i++){
        (*Lab)[i]=(char*)calloc(MaxSize, sizeof(char));
        fgets(Buf, MaxSize, source);
        strcpy((*Lab)[i], Buf);
    }
    return count;
}
int getOut(char **Lab, int x, int y, int count)
{
    int end=0;
    int i;

    Lab[y][x]='.';
    for (i=0;i<count;i++)
        printf("%s\n", Lab[i]);
    if (x==0 || y==0 || x==strlen(Lab[0])||y==count-1)
        return 1;
    if(Lab[y][x-1]==' ' && end!=1)
        end=getOut(Lab, x-1, y, count);
    if(Lab[y-1][x]==' ' && end!=1)
        end=getOut(Lab, x, y-1, count);
    if(Lab[y][x+1]==' ' && end!=1)
        end=getOut(Lab, x+1, y, count);
    if(Lab[y+1][x]==' ' && end!=1)
        end=getOut(Lab, x, y+1, count);
    if (end) return 1;
    return 0;
}


