#include <stdio.h>
typedef unsigned long long ull;
// ����� �������� ������� � ���� �������� � ������� ��������
//a - �����;
//b- ���������� �������� �� �
//c- �����

ull fib_1(ull c,ull b, ull a)
{
    if (a<=1)
    {
        return b;
    }
    else
        return (fib_1(b, b + c, a - 1));
}

ull fib_2(ull a)
{
    return (fib_1(0,1,a));
}

int main()
{
    int i;
    printf("Enter a number\n");
    scanf("%d", &i);
    printf("%d element  of the Fibonacci sequence is %lld\n", i, fib_2(i));
    return 0;
}