/*3. �������� ���������, ������� ��������� �������� �������������
����� ����� � ������ � �������������� �������� � ��� �����-����
������������ ������� ��������������
*/
#include <stdio.h>
#include <limits.h>
#define SIZE 256
unsigned long long intToString(unsigned long long, char*);
int main() {
    unsigned long long i = 0;
    char string[SIZE] = { 0 };
    printf("Enter a number (but not more than %llu!):\n", ULLONG_MAX);
    if (scanf("%llu", &i) != 1) {
        puts("Input error!");
        return -1;
    }
    intToString(i, string);
    printf("String: %s\n", string);
    return 0;
}
//����������� ������� ��������� ������ str ������� �� ����� n
unsigned long long intToString(unsigned long long n, char *str) {
    static int c = 0;
    if (c == 0) {
        unsigned long long x = n;
        while (x >= 10) {
            x /= 10;
            str++;
        }
    }
    int rem = n % 10;
    if (n < 10) {
        return n;
    }
    else {
        if (c == 0) {
            c = 1;
            *str = (intToString(n, str - 1)) + '0';
        }
        else {
            *str = (intToString(n / 10, str - 1)) + '0';
        }
    }
    return rem;
}