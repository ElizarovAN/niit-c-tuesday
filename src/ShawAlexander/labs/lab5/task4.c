/*4. �������� ���������, ������� ������ ��������� ��������� ���� �
������������ �������� ����� � ������ ������
��������� :
��������� ��������� ������������ ��������� ���� � ������ ��� ������� -
��.��� ������ ������ ���������� �������, ������������� � ������ ������
1
*/
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#define SIZE 1024
void printWord(char *pointer);
void getWords(char *pointers[], char string[]);
int totalWords = 0;

int main() {
    FILE *fp;
    char path[SIZE] = { 0 };
    char *pointers[SIZE / 2] = { 0 };
    puts("Enter a file path in the following format (C:\\file.txt):");
    fgets(path, SIZE, stdin);
    path[strlen(path) - 1] = 0;
    if ((fp = fopen(path, "r+")) == NULL)
        perror("Error");
    else {
        int lines = 1;
        char c = 0;
        //������� ���������� ����� � �����
        while ((c = fgetc(fp)) != EOF) {
            if (c == '\n')
                lines++;
        }
        fseek(fp, 0, SEEK_SET);

        char string[SIZE] = { 0 };
        for (int i = 0; i < lines; i++) {
            fgets(string, SIZE, fp);
            if (string[strlen(string) - 1] == '\n')
                string[strlen(string) - 1] = 0;
            getWords(pointers, string);
            srand(time(0));
            for (int i = 0; i < totalWords;)
            {
                int randWord = (rand() % totalWords);
                if (pointers[randWord] != NULL) {
                    printWord(pointers[randWord]);
                    pointers[randWord] = NULL;
                    i++;
                }
                else {
                }
            }
            putchar('\n');
            totalWords = 0;
        }

    }
    return 0;
}

void printWord(char *pointer) {
    char c;
    while (!isspace(c = *pointer) && c != 0) {
        putchar(c);
        pointer++;
    }
    putchar(' ');
    return 0;
}

void getWords(char *pointers[], char string[]) {
    short startFound = 0;
    int pointersPos = 0, startPos = 0;
    char c;
    for (int i = 0; i < strlen(string); i++)
    {
        char c = string[i];
        if (!isspace(c) && !startFound) {
            startFound = 1;
            pointers[pointersPos] = &string[i];
            pointersPos++;
            totalWords++;
        }
        else if (isspace(c) && startFound) {
            startFound = 0;
        }
    }
    return 0;
}