/* Написать программу, которая читает построчно текстовый файл и
переставляет случайно слова в каждой строке*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funct.c"
#define N 100
#define M 100
#define W 30

int main() {
	int i = 0, count = 0, j = 0, k = 0;
	char text[N][M] = { 0 };
	int *p_num[M] = { 0 };
	char*p_words[W] = { 0 };
	int*p_j = &j;
	// Ввод данных
	FILE*in = fopen("Input.txt", "rt");
	if (in == 0) {
		perror("File:");
		return 1;
	}
	while (fgets(text[count], N, in) != '\n') {
		if (text[count][0] == '\0') break;
		count++;
	}
	// Cортировка и вывод
	for (i = 0; i < count; i++) {
		getWords(p_words, &text[i], p_j);
		ReBuilder(p_words, j);
		putchar('\n');
		memset(p_words, 0, sizeof(p_words));		
	}	
	return 0;
}