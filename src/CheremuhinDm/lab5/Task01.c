/*Написать программу, которая принимает от пользователя строку и
выводит ее на экран, перемешав слова в случайном порядке.*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "funct.c"
#define N 100

int main() {
	int i = 0, j = 0;
	char text[N] = { 0 };
	char*p_text[N] = { 0 };
	int*p_j = &j;

	puts("Type your text: ");
	if (fgets(text, N, stdin) != ' ') {
		getWords(p_text, &text, p_j);
		Randomizer(p_text, j);
		for (i = 0; i < j; i++) {
			printWord(p_text[i], '\n');
		}
	}
	return 0;
}