#include "functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void chomp(char *str)
{
    if(str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=0;
}
P_WORD_LIST makeTree(P_WORD_LIST root, char * str)
{
    int i=0;
    if (root==NULL)
    {
        root=(P_WORD_LIST)malloc(sizeof(T_WORD_LIST));
        root->count=0;
        root->left=NULL;
        root->right=NULL;
        while (str[i])
        {
            root->word[i]=str[i];
            i++;
        }
        root->word[i]=0;
        return root;
    }
    else if (strcmp(root->word, str)>0)
        root->left=makeTree(root->left, str);
    else if (strcmp(root->word, str)<0)
        root->right=makeTree(root->right, str);
    return root;
}
P_WORD_LIST seachTree(P_WORD_LIST root, char * str)
{
    if(strcmp(root->word, str)==0){
        root->count++;
        return root;
    }
    if(strcmp(root->word, str)>0)
        if(root->left==NULL)
            return NULL;
        else
            return seachTree(root->left, str);
    else
        if(root->right==NULL)
            return NULL;
        else
            return seachTree(root->right, str);
}
void printTree(P_WORD_LIST root)
{
    if(root->left!=NULL)
        printTree(root->left);
    printf("%s - %d\n", root->word, root->count);
    if(root->right!=NULL)
        printTree(root->right);
}

