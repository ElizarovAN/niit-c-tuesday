#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct WORD_LIST
{
    char word[256];
    int count;
    struct WORD_LIST * left;
    struct WORD_LIST * right;
};
typedef struct WORD_LIST T_WORD_LIST;
typedef T_WORD_LIST * P_WORD_LIST;

void chomp(char *str);
P_WORD_LIST makeTree(P_WORD_LIST root, char * str);
P_WORD_LIST seachTree(P_WORD_LIST root, char * str);
void printTree(P_WORD_LIST root);

