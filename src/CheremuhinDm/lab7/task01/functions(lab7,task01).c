#include "task01.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

PITEM createList(PNAME_REC name_rec){
	PITEM item = (PITEM)malloc(sizeof(TITEM));
	item->name_rec=name_rec;
	item->prev=NULL;
	item->next=NULL;
	return item;
}

PNAME_REC createName(char * line){
	int i = 0;
	PNAME_REC rec=(PNAME_REC)malloc(sizeof(TNAME_REC));
	while(*line && *line!=',') rec->name[i++]=*line++;
	rec->name[i]=0;
	line++;
	i = 0;
	while(*line && *line!=',') rec->numcode[i++]=*line++;
	rec->numcode[i]=0;
	line++;
	i = 0;
	while (*line) rec->meaning[i++] = *line++;
	rec->meaning[i]=0;
	return rec;
}

PITEM addToTail(PITEM tail, PNAME_REC name_rec){
	PITEM item = createList(name_rec);
	if(tail!=NULL){
		tail->next = item;
		item->prev = tail;
	}
	return item;
}

int countList(PITEM head){
	int count = 0;
	while (head) {
		count++;
		head=head->next;
	}
	return count;	
}

PITEM findByName(PITEM head, char *name){
	int count = 0;
	PITEM start;
	start = head;
	while(head) {
		if(strcmp(head->name_rec->name, name)==0) {
			printName(head);
			count++;
		}
		head=head->next;
	}
	if (count>0) return start;
	else return NULL;
}

void printName(PITEM item){
	if (item!=NULL) {
		printf("%s, %s, %s", item->name_rec->name, item->name_rec->numcode, item->name_rec->meaning);		
	}
}