#include "functions.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    FILE *fp_zzz, *fp_o, *fp;
    PSYM root;          //������ ������
    TSYM syms[256]={0};
    PSYM psyms[256];
    char buf [256]={0};
    char Fname[256]={0};
    int i=0, j=0;
    long ii;            //�������
    int count=0;        //���������� ���������� ��������
    char tail=0;        //����� ������
    long len=0;         //����� �����

    if(argc<2){
        puts("Input Error!");
        exit (0);
    }
    if (!(fp_zzz=fopen(*(argv+1), "rb"))){
        perror("file:");
        exit (1);
    }
    fread(buf, sizeof(char), 3, fp_zzz);
    i=strcmp(buf, "zzz");
    if(i){
        puts ("Wrong file format");
        exit (2);
    }
    //���������� ���������� ��������
    fread(&count, sizeof(char), 1, fp_zzz);
    count++;
    //��������� ������ ������/�������
    for (i=0; i<count; i++){
        fread(&syms[i].ch, sizeof(char), 1, fp_zzz);
        fread(&syms[i].freq, sizeof(syms[i].freq), 1, fp_zzz);
        psyms[i]=&syms[i];
    }
    //����� � ����� �����
    fread(&tail, sizeof(tail), 1, fp_zzz);
    fread(&len, sizeof(len), 1, fp_zzz);
    //������� ������
    root=buildTree(psyms, count);
    makeCodes(root);
    //������ ���������� ����� �� ��������
    i=strlen(*(argv+1));
    while(*(*(argv+1)+i)!='.')
        i--;
    for(j=0; j<i;j++)
        Fname[j]=*(*(argv+1)+j);
    Fname[i]=0;
    fp=fopen(Fname, "wb");
    fp_o=fopen("temp", "w+t");
    //������� ���� �� 0 � 1
    while(fread(buf,sizeof(char),1, fp_zzz))
    {
        unpack(buf);
        fwrite(buf,sizeof(char), 8, fp_o);
    }
    rewind(fp_o);
    //���������� ����
    for (ii=0; ii<len; ii++)
        decode(fp_o, fp, root, len);
    i=0;
    //��������� �������
    while(fread(buf, sizeof(char), 1, fp_o))
        i++;
    if(tail!=(8-i))
        puts("ERROR! Wrong end of file!");
    fcloseall();
    return 0;
}
