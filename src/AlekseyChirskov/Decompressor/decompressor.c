#include "decompressor.h"

PSYM buildTree(PSYM *psym, int N)
{
	int i,j;
	PSYM temp=(PSYM)malloc(sizeof(TSYM));
	temp->freq=psym[N-2]->freq+psym[N-1]->freq;
	temp->left=psym[N-1];
	temp->right=psym[N-2];
	temp->code[0]=0;
	if(N==2)
	return temp;
	else{
		for(i=0;i<N;i++)
			if(temp->freq>psym[i]->freq)
			{   
				for(j=N-1;j>i;j--)
					psym[j]=psym[j-1];
				psym[i]=temp;
				break;
			}       
	}
	return buildTree(psym,N-1);
}
void makeCodes(PSYM node)
{
	if(node->left)
	{
		strcpy(node->left->code,node->code);
		strcat(node->left->code,"0");
		makeCodes(node->left);
	}
	if(node->right)
	{
		strcpy(node->right->code,node->code);
		strcat(node->right->code,"1");
		makeCodes(node->right);
	}
}
int comp(const void *a, const void *b)
{
	return (*(PSYM)b).freq - (*(PSYM)a).freq;
}
void  pack(UC *buf)
{
	union CODE code;
	code.ch=buf[0];
	buf[0]=code.byte.b1+'0';
	buf[1]=code.byte.b2+'0';
	buf[2]=code.byte.b3+'0';
	buf[3]=code.byte.b4+'0';
	buf[4]=code.byte.b5+'0';
	buf[5]=code.byte.b6+'0';
	buf[6]=code.byte.b7+'0';
	buf[7]=code.byte.b8+'0';
}
void comeback(FILE *bin_file, FILE *fp, PSYM node)
{
    int c;
	if(!node->left)
		fputc(node->ch,fp);
    else
	{
		while((c=fgetc(bin_file))!=EOF)
        {
			if(c=='0')
				return comeback(bin_file,fp,node->left);
			else
				return comeback(bin_file,fp,node->right);
		}	
	}
}
long getFileLength(FILE *file)
{
	long length;
	rewind(file);
	fseek(file,0,SEEK_END);
	length=ftell(file);
	fclose(file);
	return length;
}