#include "functions.h"

int CreateArr(FILE * fp, PSYM Arr, PSYM * PArr)
{
    unsigned int count=0;
    int c=0;
    rewind(fp);
    while (fread(&c, sizeof(char), 1, fp))
    {
        Arr[c].ch=(char)c;
        Arr[c].freq++;
        count++;
    }
    qsort(Arr, 256, sizeof(TSYM), cmp);
    c=0;
    while(Arr[c].freq>0 && c<256){
        PArr[c]=Arr+c;
        c++;                                //������� ���������� ���������� ���������
    }
    return c;
}

int cmp(const void *a, const void *b) 
{
    return (*(TSYM*)b).freq - (*(TSYM*)a).freq;
}

PSYM buildTree(PSYM psym[], int N)
{
    // ������ ��������� ����
    PSYM temp=(PSYM)malloc(sizeof(TSYM));
    int i;
    // � ���� ������� ������������ ����� ������
    // ���������� � �������������� ��������� ������� psym
    temp->freq=psym[N-2]->freq+psym[N-1]->freq;
    // ��������� ��������� ���� � ����� ���������� ������
    temp->left=psym[N-1];
    temp->right=psym[N-2];
    temp->code[0]=0;
    if(N==2) // �� ������������ �������� ������� � �������� 1.0
        return temp;
    else
    {
        // ��������� temp � ������ ������� psym,
        // �������� ������� �������� �������
        i=N-2;
        while(temp->freq>psym[i]->freq)
        {
            psym[i+1]=psym[i];
            i--;
            if (i<0)
                break;
        }
        psym[i+1]=temp;
    }
    return buildTree(psym,N-1);
}

void makeCodes(PSYM root)
{
    if(root->left)
    {
        strcpy(root->left->code,root->code);
        strcat(root->left->code,"0");
        makeCodes(root->left);
    }
    if(root->right)
    {
        strcpy(root->right->code,root->code);
        strcat(root->right->code,"1");
        makeCodes(root->right);
    }
}

unsigned char pack(unsigned char buf[])
{
    union CODE code;
    code.byte.b1=buf[0]-'0';
    code.byte.b2=buf[1]-'0';
    code.byte.b3=buf[2]-'0';
    code.byte.b4=buf[3]-'0';
    code.byte.b5=buf[4]-'0';
    code.byte.b6=buf[5]-'0';
    code.byte.b7=buf[6]-'0';
    code.byte.b8=buf[7]-'0';
    return code.ch;
}
