#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int intPower(int a, int b){ // Возводит нат. число в степень. a - нат. число, b - степень числа
	int i, rez = 1;
	if (b == 0) return 1;
	if (b == 1) return a;
	if (b < 0) { // данную ветку оставляю для возможного переделывания этой функции в тип float и т.п.
		for (i=1; i<=abs(b); i++){ // при отриц. b будет возвращать как при b == 0.
			rez *= a;
		}
		rez = 1/rez;
		return rez;
	} 
	if (b > 1){
	    for(i = 1; i <= b;) {
	        rez *= a;
			i++;
	    }
	    return rez;
	}
}

int getNumber(int min, int max){
	int number = 0;
	while (1){
				printf("Please, enter the number of iterations(from %d to %d):\n", min, max);
				scanf("%d", &number);
				if (number < min || number > max){
					puts("Input error!\n");
					clean_stdin();
				}
				else break;
			}
return number;
}

void arrPrinter(char**FractArr, int side){ // Печатает содержимое дин. массива в строку.
	int i = 0, j = 0;

	for(i = 0; i < side; i++) {
		for(j = 0; j < side; j++) putchar(FractArr[i][j]);
		putchar('\n');
	}
}

void arrPrinterToFile(char**FractArr, int side){ // Печатает содержимое дин. массива в файл.
	int i = 0, j = 0;
	FILE*file;
	file = fopen("Fractal.txt", "wt");
	for(i = 0; i < side; i++) {
		for(j = 0; j < side; j++) putc(FractArr[i][j], file);
		putc('\n', file);
	}
	fclose(file);
	system("C:\\windows\\notepad.exe Fractal.txt");		
}

void arrGenerator(char**p, int count, int number, char sign){ // Генерирует дин. массив и заполняет его фракталом
	char**FractArr = NULL;
	int i = 0, j = 0, side = 0, delta = 0;
	
	side = intPower(3,count);
	FractArr=(char**)malloc(side * sizeof(char*));
	for(i = 0; i < side; i++){
		FractArr[i]=(char*)malloc(side * sizeof(char));
		memset(FractArr[i], ' ', side * sizeof(char));
	}
	if(count == 0) **FractArr = sign;
	if(count > 0) {
		delta = side/3;
		for(i = 0; i < delta; i++)
			for (j = 0; j < delta; j++){
				FractArr[delta+i][delta+j] = p[i][j];
				FractArr[i][delta+j] = FractArr[delta+i][delta+j];
				FractArr[2*delta+i][delta+j] = FractArr[delta+i][delta+j];
				FractArr[delta+i][j] = FractArr[delta+i][delta+j];
				FractArr[delta+i][2*delta+j] = FractArr[delta+i][delta+j];
			}
	}
	if(count == number){ 
		if (number<=3) arrPrinter(FractArr, side); // Для печати в ком. строку.
		else arrPrinterToFile(FractArr, side); // Для печати в файл.
	}
	else { 
		count++;
		arrGenerator(FractArr, count, number, sign);
	}
	for(i = 0; i < side; i++) free(FractArr[i]); // Очистка массива созданного на данном уровне рекурсии.
	free (FractArr);
}