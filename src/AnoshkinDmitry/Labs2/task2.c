/*
�������� ��������� ������� �����. ��������� ���������� ����� � �����-
���� �� 1 �� 100 � ������������ ������ ������� ��� �� ���������� ����������
�������.
���������:
������������ ������ �����, � ��������� ������������ ���: �������, �����-
��, �������!�
*/

#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define MaxValue 100

void clean_stdin()  // ������� ������� ������
{
     int c;
     do
     {
         c=getchar();
     }
     while (c!='\n' && c!=EOF);
}

int main()
{
int value, inpvalue;

srand(time(0));             //����� ��������������� ����� ���� ���������� (����a� ������������������)
value=rand()%100+1;
puts ("Try to guess nomber from 1 to 100");
	while(1)
	{
        if (scanf("%d", &inpvalue)<1)
        {
            puts ("Input Error!");
            clean_stdin();
        }
        else
            if (value>inpvalue) 
                puts ("It is bigger");
            else if (value<inpvalue)
                puts ("It is smaller");
            else
            {
                puts ("Congratulations, You did it!");
                break;
            }
	}
return 0;	
}