// Калькулятор. Подсчет строки
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

int Validator(char var) { // 
	if (var >= 48 && var <= 57) return 1;
	if (var == '+' || var == '-' || var == '*' || var == '/') return 2;
	if (var == '(' || var == '(') return 3;
	else return 0;
}

int Eval(char sign, int op1, int op2) {
	switch(sign) {
		case '+': 
			return (op1+op2);
			break;
		case '-': 
			return (op1-op2);
			break;
		case '*': 
			return (op1*op2);
			break;
		case '/': 
			return (op1/op2);
			break;
		default:
			puts("Error!");
	}
}

int Partition(char*buff, int lenght) {
	int i = 0, count = 0, j = 0, flag = 0;
	char sign, val1[50] = {0}, val2[50] = {0};
	for(i = 0; i < lenght; i++)	{
		if (buff[i] == '(') {
			count++;
			continue;
		}
		if (buff[i] == ')') {
			count--;
			continue;
		}
		if (Validator(buff[i]) == 1 && buff[i+1] == ')' && buff[i-1] == '(' && lenght == 3) return atoi(&buff[i]);
		if (Validator(buff[i]) == 2 && count == 1) {
			sign = buff[i];
			for(j = 1; j < i; j++) {
				val1[j-1] = buff[j];
			}
			for(j = 0; j+i < lenght-2; j++) {
				val2[j] = buff[j+i+1];
			}
			flag = 1;
		}		
		if (flag == 1) {
			if (strlen(val1) == 1 && strlen(val2) == 1) return Eval(sign, atoi(val1), atoi(val2));
			if (strlen(val1) == 1 && strlen(val2) > 1) return Eval(sign, atoi(val1), Partition(val2, strlen(val2)));
			if (strlen(val1) > 1 && strlen(val2) == 1) return Eval(sign, Partition(val1, strlen(val1)), atoi(val2));
			if (strlen(val1) > 1 && strlen(val2) > 1) return Eval(sign, Partition(val1, strlen(val1)), Partition(val2, strlen(val2)));
		}
	}	
}

int main(int argc, char*argv[]) {	
	if(!argv[1]) {
		puts("Input error!");
		getch();
		return 1;
	}	
	printf("\n%d\n" , Partition(argv[1], strlen(argv[1])));
	return 0;
} 



/*int argvCheck(char*buff, int lenght) {
	int count = 0, nums = 0, signs = 0, i = 0;
	for(i = 0; i < lenght; i++)	{
		if (buff[i] == '(') {
			if (Validator(buff[i+1]) == 2) return 0;
			count++;
			continue;
		}
		if (buff[i] == ')') {
			if (Validator(buff[i-1]) == 2) return 0;
			count--;
			continue;
		}
		if (Validator(buff[i]) == 0) return 0;
		if (Validator(buff[i]) == 1) nums++;
		if (Validator(buff[i]) == 2) signs++;
	}
	if (count != 0) return 0;
	if (nums-signs != 1 && lenght > 3) return 0;
    return 1;
}*/