/*
�������� ���������, ������� ������� ����� ����� �� �������� ������
���������:
��������� ������������� ����������� ������������������ ���� � ������ ���
����� � ������������ �� ��� ������ �����. � ��������� ������������� ������-
����� �� ������������ ����� ��������, �� ���� ���� ������������ ������ �����
������� ������������������ ����, � ����� ������� �� ��������� �����.
*/

//By Anoshkin D.M.

#include <stdio.h>
#include <string.h>

#define MaxLen 80
#define MaxLenN 4   //����������� ��� ��� � 32 ���������
int main()
{
    char str[MaxLen]={0};
    char inNum=0;
    char i=0, j=0;
    unsigned int sum=0, sumi=0;

    puts("Enter a line:");
    fgets(str, MaxLen, stdin);
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=' ';

    while (str[i])
    {
        if (str[i]>='0' && str[i]<='9' && j<MaxLenN)
        {
            sumi=sumi*10+(str[i]-'0');
            j++;
        }
        else
        {
            sum=sum+sumi;
            sumi=0;
            if(j==4) i--;       //��������� i ����� �� �������� �����
            j=0;
        }
        i++;
    }
    printf ("Sum = %u \n", sum);
    return 0;
}
