/*
�������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������:
����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������.
*/
//by Anoshkin Dmitry
#include <stdio.h>

#define MaxFamilies 20

int main()
{
    int N=0;  //���������� �������������
    int i=0;
    int age=0, age_max=0, age_min=100;
    char *young=0, *old=0;
    char family [MaxFamilies][80]={0};

    puts("How much families do u have?");
    while (scanf ("%d", &N)==0 || N>MaxFamilies || N<=0)
        fflush (stdin);
    fflush (stdin);
    puts("Enter name and age like 'Tony 25'");
    for (i=0; i<N; i++)
    {
        while(scanf("%s %d", family[i], &age)<2 || age<0 || age>100)
            fflush(stdin);
        if (age>age_max)
        {
            age_max=age;
            old=family[i];
        }
        if (age<age_min)
        {
            age_min=age;
            young=family[i];
        }
    }
    if (age_min<100)
        printf("The youngest is %s \nThe oldest is %s\n", young, old);
    return 0;
}
