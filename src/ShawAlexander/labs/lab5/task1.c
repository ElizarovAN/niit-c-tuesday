/* 1. �������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
*/
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#define SIZE 1024
void printWord(char *pointer);
void getWords(char *pointers[], char string[]);
int totalWords = 0;

int main() {          
    char string[SIZE] = { 0 };
    char *pointers[SIZE / 2] = { 0 };
	puts("Enter a string:");
    fgets(string, SIZE, stdin);
    getWords(pointers, string);
    srand(time(0));
    for (int i = 0; i < totalWords;)
    {
        int randWord = (rand() % totalWords);
        if (pointers[randWord] != NULL) {
            printWord(pointers[randWord]);
            pointers[randWord] = NULL;
            i++;
        }
        else {
        }
    }
    return 0;
}

void printWord(char *pointer) {
    char c;
    while (!isspace(c = *pointer)) {
        putchar(c);
        pointer++;
    }
    putchar(' ');
    return 0;
}

void getWords(char *pointers[], char string[]) {
    short startFound = 0;
    int pointersPos = 0, startPos = 0;
    char c;
    for (int i = 0; i < strlen(string); i++)
    {
        char c = string[i];
        if (!isspace(c) && !startFound) {
            startFound = 1;
            pointers[pointersPos] = &string[i];
            pointersPos++;
            totalWords++;
        }
        else if (isspace(c) && startFound) {
            startFound = 0;
        }
    }
    return 0;
}