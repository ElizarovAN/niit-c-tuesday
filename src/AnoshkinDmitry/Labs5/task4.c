/*
4. �������� ���������, ������� ������ ��������� ��������� ���� �
������������ �������� ����� � ������ ������
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ���������� �������, ������������� � ������ ������
1.
*/
//by Anoshkin Dmitry


#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

#define MaxLen 80

int getWords (char*strs, char**pstn, int len)
{
    int count=0;
    int i=0;
    while(*(strs+i)!=10 && *(strs+i)!=0)
    {
        while(*(strs+i)==' ') i++;
        if (*(strs+i)!=' '&& *(strs+i)!=10 && *(strs+i)!=0)
        {
            pstn[count]=strs+i;
            count++;
            while(*(strs+i)!=' '&& *(strs+i)!=10 && *(strs+i)!=0) i++;
        }
    }
    return count;
}

void printWord (char*pstn)
{
    int i=0;
    while (*(pstn+i)!=' '&&(*(pstn+i))!='\n')
        putchar(*(pstn+i++));
    putchar (' ');
    return 0;
}

int main()
{
    FILE *in;                               // ��������� �� ����
    char strs[MaxLen]={0};                  
    char *pstn[MaxLen/2]={strs};
    int count=0, rnd;
    int i, j;
    
    srand(time(NULL));
    in=fopen("in.txt", "rt");

    while (fgets(strs, MaxLen, in)!=0)
    {
        count=getWords (strs, pstn, MaxLen/2);
        for (i=0;i<count;i++)
        {
            rnd=rand()%(count-i);
            printWord (pstn[rnd]);
            for (j=rnd; j<count-1; j++)
                pstn[j]=pstn[j+1];
        }
       printf("\n");
    }

    return 0;

}
