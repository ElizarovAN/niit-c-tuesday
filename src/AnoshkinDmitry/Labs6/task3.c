/*
3. �������� ���������, ������� ��������� �������� �������������
����� ����� � ������ � �������������� �������� � ��� �����-����
������������ ������� ��������������
*/

//by Anoshkin Dmitry

int get_string_of_number (char *str, int n, int i)
{
    int pstn=i;
    if (str[0]=='-') pstn--;
    if (n>0)
        if (n<10)
            str[i-pstn]='1'-1+n%10;
        else
        {
            i=get_string_of_number(str, n/10, ++i);
            str[i-pstn]='1'-1+n%10;
        }
    return i;
}
void string_of_number (char *str, int n)
{
    int i=0;
    if (n<0)
    {
        str[0]='-';
        i=get_string_of_number (str, n*(-1), 1);
    }
    else if (n==0)
        str[0]='0';
    else
        i=get_string_of_number (str, n, 0);
    
    str[i+1]=0;
}


int main()
{
    int k;
    char str[10]={0};
    puts ("Enter number");
    if (scanf ("%d", &k))
    {
        string_of_number (str,k);
        printf ("%s \n", str);
    }
    else 
        printf("Wrong number");
    return 0;
}