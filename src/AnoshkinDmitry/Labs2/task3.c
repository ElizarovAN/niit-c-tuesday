/*
�������� ���������, ��������� �� ����� ����������� �� ��������
���������:
����������� ������ ��������� ���:
*
***
*****
���������� ����� ������� ������������� � ����������
*/

//by Anoshkin Dmitry

#include <string.h>
#include <stdio.h>

#define MaxHeight 20

int main()
{
    int H;
    int i, j;  // index
    
    printf ("Input the height (1 to %d)\n", MaxHeight);
    if (scanf ("%d", &H)<1 || H<1 || H>MaxHeight)
    {
        puts ("Input Error!");
        return 1;
    }
 
    for (i=0; i<H; i++)                //���� �� ������
    { 
        for(j=1;j<H-i;j++)
            putchar(' ');
        for (;j<=H+i;j++)
            putchar('*');
        putchar('\n');        
    }
    return 0;
}