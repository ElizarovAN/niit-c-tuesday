#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

void charChanger(char *a, char *b) {
	char*tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}

void getWords(char**p_text[],char*text, int*p_j) {
	int i = 0, j = 0;
	for (i = 0; i < strlen(text); i++) {
		if (*((text)+i) != ' ' && *((text)+i) != '\n') {
			p_text[j] = ((text)+i);
			j++;
		}
		while (*((text)+i) != ' ' && *((text)+i) != '\n' && *((text)+i) != '\0') i++;
	}	
	*p_j = j;
	}

void getChars(char*p_symb[], char*p_words) {
	int i = 0, j = 0;
	char*tmp = 0;
	for (i = 0; (*(p_words)+i) != ' ' || (*(p_words)+i) != '\n' || (*(p_words)+i) != '\0'; i++) {
			p_symb[j] = p_words+i;
			tmp = (p_words)+i;
			tmp = p_symb[j];
			if (p_symb[j] == '\0') break;
			j++;			
		}		
	}	


void Randomizer(char*a[], int n) {
	int dig = 0, i = 0;
	char*tmp = 0;
	srand(time(NULL));
	for (i = 0; i < n; i++) {
		dig = rand() % n;
		tmp = a[i];
		a[i] = a[dig];
		a[dig] = tmp;
	}
}

void printWord(char*a, char b) {
	int i = 0;
	while (1) {
		if (*((a)+i) == b || *((a)+i) == '\n' || *((a)+i) == '\0') break;
		putchar(*((a)+i));
		i++;
	}
	putchar(' ');
}

void AddSign(char *p_arr, char x, int arrH, int arrW){
	int i = 0;
	srand(time(0));
	while (i<arrH*arrW){
		if(rand()%2==1) *(p_arr+i)=x;
		i++;
	}	
	*(p_arr+i)='\0';
}

void StrPrnt(char*p_arr, int H, int W){
	int i=0;
	for (;i<H*W/2;){
		do{
			putchar(*(p_arr+i));
			i++;
		}while(i%W!=0); 
		i--;
		do{
			putchar(*(p_arr+i));
			i--;
		}while(i%W!=0);
		putchar(*(p_arr+i));
		putchar('\n');
		i=i+W;
	}
	for (;i>0;){
		i=i-W;
		do{
			putchar(*(p_arr+i));
			i++;
		}while(i%W!=0); 
		i--;
		do{
			putchar(*(p_arr+i));
			i--;
		}while(i%W!=0);
		putchar(*(p_arr+i));
		putchar('\n');
	}
}

void Spacer(char*p_arr, int L){
	int i;
	for (i=0;i<L;i++) *(p_arr+i)=' ';
}

void ReBuilder(char*a[], int n){
	int i = 0, j = 0, count = 0, dig = 0;
	char tmp = 0;
	srand(time(NULL));
	for(i = 0; i < n; i++){
		count = 0;
		while (*(a[i]+count)!= ' ' && *(a[i]+count)!= '\n' && *(a[i]+count)!= '\0') count++;
		for(j=1;j<count-1;){
			dig = rand() % (count-2)+1;
			tmp = *(a[i]+j);
			*(a[i]+j) = *(a[i]+dig);
			*(a[i]+dig) = tmp;
			j++;
		}
		for(j=0;j<count;j++) putchar(*(a[i]+j));
		putchar(' ');
	}		
}