#include "trees.h"

int main(int argc, char *argv[]) {
    if (argc < 2) {
        puts("Usage: program_name file_path");
        exit(1);
    }
    FILE *kwFile = fopen("keywords.txt", "rt");
    PNODE root = makeTree(kwFile);
    fclose(kwFile);
    FILE *cFile = fopen(argv[1], "rt");
    char buf[128] = { 0 };
    PNODE node = NULL;
    while (fscanf(cFile, "%s", buf) != EOF) {
        if ((node = searchTree(root, buf)) != NULL)
            (*node).timesUsed++;
    }
    printTree(root);
    getch();
    return 0;
}