/*�������� ���������, ������� ��������� ���� �� ������������ ��-
����� (����, �����) � ����������� (����������). ������ ���-
����� � ���� ���� ����� �����, ��������� � ���� �������������
����� � ��������� �� 1 �����. 1 ��� = 12 ������. 1 ���� = 2.54
��.*/
#include <stdio.h>
int main() 
{
	float foot,inch,cm;
	printf("Enter value: ");
	scanf("%f%f",&foot,&inch);
	printf("%.0f foot %.0f inch = ",foot,inch);
	foot=inch*12;
	cm=foot+inch*2.54;
	printf("%0.1f cms\n",cm);
	return 0; 
}