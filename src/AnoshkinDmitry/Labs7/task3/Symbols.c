#include "Symbols.h"
#include <stdio.h>
#include <stdlib.h>

TSYM * CreateArr(FILE *fp)
{
    int count =0;   //total elements
    int c;
    TSYM * Arr = (TSYM*)calloc(256, sizeof(TSYM));
    while ((c=fgetc(fp))!=EOF)
    {
        Arr[c].sym=(char)c;
        Arr[c].freq++;
        count++;
    }
    qsort(Arr, 256, sizeof(TSYM), cmp);
    for(c=0; c<256; c++){
        Arr[c].freq=Arr[c].freq/count;
    }
    return Arr;
}

int cmp(const void *a, const void *b) 
{
    return (*(TSYM*)b).freq - (*(TSYM*)a).freq;
}

