// Программа генерирует дин. массив, заполняет его фракталом и выводит
// в командную строку и в файл.
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void arrGenerator(char**p, int count, int number, char sign);
int getNumber(int min, int max);

int main(){
	int i = 0, j = 1, count = 0, number = 0;	
	//Ввод числа итераций
	number = getNumber(0, 6); // При большем числе итераций происходит смещение строк из-за ограничения дины строки в notepad.
	//Генерация и вывод
	arrGenerator( NULL, 0, number, '*');
	return 0;
}