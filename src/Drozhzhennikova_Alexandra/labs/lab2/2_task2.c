#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	clock_t now;
	int value, number;
	srand(time(0));
	value = rand() % 100 + 1;    
	while (1)
	{
		printf("chose the number\n");
		scanf("%d", &number);
		if (number == value)
			printf("you have guessed right\n");
		if (number > value)
			printf("bigger than value\n");
		if (number < value)
			printf("less than value\n");
		now = clock();
		while (clock() < now + 1000);
		
	}
	return 0;
}