/*Написать программу, которая переводит введённое пользователем
целое число в строку с использованием рекурсии и без каких-либо
библиотечных функций преобразования*/
#include <stdio.h>
#include <stdlib.h>
#include "task03.h"

typedef unsigned int UI;

void Arithm(char sign, struct BUFFER *buffer); // Считывает введенный оператор и возвращает в структуру результат
int Validator(struct BUFFER *buffer); // Проверяет введённые данные
void clean_stdin();
void Converter(struct BUFFER *buffer); // Конвертирует в тип int

int main() {
	int index = 0;
	char sign = '0';
	struct BUFFER *buffer;
	buffer = (struct BUFFER*)calloc(1, sizeof(struct BUFFER));
	while(1) {
		puts("Enter the number:\n");
		scanf("%s", buffer->arr);
		buffer->order = strlen(buffer->arr);
		if (Validator(buffer) == 0) break;
		else {
			puts("Error!");
			clean_stdin();
		}
	}
	Converter(buffer);
	printf("\n\nThe result is %d\n\nTry it!\nEnter the operator!('+', '-', '*' or '/'):\n", buffer->result);	
	clean_stdin();
	scanf("%c", &sign);
	printf("Enter the second number: \n");
	scanf("%d", &index);	
	Arithm(&sign, &index, buffer);
	printf("The result is %d\n\n", buffer->result);
	return 0;
}