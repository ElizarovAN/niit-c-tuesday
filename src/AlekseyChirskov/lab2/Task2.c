/*�������� ��������� ������� �����. ��������� ���������� ����� � �����-
���� �� 1 �� 100 � ������������ ������ ������� ��� �� ���������� ����������
�������.
���������:
������������ ������ �����, � ��������� ������������ ���: �������, �����-
��, �������!�.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 100//�������� ����� �� 1 �� N
void clean_stdin()
{
	int c;
	do {
		c=getchar();
	} while (c!='\n' && c!=EOF);
}
int main()
{
	int number,chislo;
	srand (time(NULL));
	chislo = rand() % N + 1;   
	printf("%d\n",chislo);//���������� ���������� ����� ��� ��������
	do {
		printf("Put your number\n");
		if(scanf("%d",&number)<1)
		{
			printf("Error! Try again please!\n");
			clean_stdin();
		}
		else if(number==chislo)
			printf("Your are a winner!\n");
		else if(number<chislo)
			printf("Need bigger\n");
		else if(number>chislo)
			printf("Need smaller\n");
	} while(number!=chislo);
	return 0;
}