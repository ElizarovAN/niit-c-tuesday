/*
  �������� ���������, ������� ��������� ������������ ������ �������-
�� ����� � ����������, � ����� ��������� �� � ������� ��������-
��� ����� ������.
���������:
������ �������� �� ��������� ������ ������ � ������������ � ���������
���������� ������. ������������ ���������� �������� � ���������� �������
���������� �� char. ����� ��������� ����� ��������� ��������� ��������� �
������� � ������� ������ � ������������ � ���������������� �����������.
*/
//by Anoshkin Dmitry

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MaxCount 20 //max count of strings
#define MaxLen 80 //max length of strings

int comp(const void *a, const void *b)          //��������� ��������� �� ���������
{
    return strlen(*(char**)a)-strlen(*(char**)b);
}

int main()
{
    char strs[MaxCount][MaxLen]={0};        //strings array
    char *str[MaxCount]={0};                //pointers for strings
    int i=0, count=0;
    
    puts ("Input your strings");
    while (*fgets(strs[count], MaxLen, stdin)!='\n' && count<MaxCount)
    {
        str[count]=strs[count];           //����������� ���������� ������
        count++;
    } 
    qsort(str, count, sizeof(char*), comp);          //comp ���� ������� ����������

    for (i=0; i<count; i++)
        printf("%s", str[i]);

    return 0;
}