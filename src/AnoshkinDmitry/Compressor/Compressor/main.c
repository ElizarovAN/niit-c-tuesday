#include "functions.h"

int main(int argc, char *argv[])
{
    FILE *fp, *fp_101, *fp_zzz;
    TSYM syms[256]={0};       //������ �������� ���� SYM
    PSYM psyms[256];          //������ ���������� �� �������� ������� Arr
    PSYM root;                //������ ������
    char newFname[256]={0};   //��� ������ ����� �����
    char buf[8];              //ext - ��� �������� ���������� �����
    int count=0, i, j;        //count - ���������� ���������� ��������
    long len=0, tmp_tail=0;   //len - ����� �����
    int ch;                   // ��� ������� �� �����
    char tail=0;

    if (argc!=2) 
    {
        puts ("Input Error!");
        exit (1);
    }
    fp=fopen(*(argv+1), "rb");
    if (!fp)
    {
        perror("file:");
        exit (2);
    }
    if (!(count=CreateArr(fp, syms, psyms))){
        puts("Array Error!");
        exit(3);
    }
    root=buildTree(psyms, count);   //������� ������
    makeCodes(root);                //������� ���� ��������
    
    rewind(fp);

    //�������� ���������� ����� ��� 01
    i=0;
    while(*(*(argv+1)+i)!=' '&&
           *(*(argv+1)+i)!=0)
    {   
        newFname[i]=*(*(argv+1)+i);
        i++;
    }
    newFname[i]='.';                //��������� � ���� .�
    newFname[i+1]='o';
    newFname[i+2]=0;
    j=i;                            //��������� ��������� "."
    fp_101=fopen(newFname, "w+");
    //���������� � ���� 0 � 1;
    while((ch=fgetc(fp))!=-1)
    {
        for(i=0;i<count;i++)
            if(syms[i].ch==(unsigned char)ch) {
                fputs(syms[i].code,fp_101);    // ������� ������ � �����
                tmp_tail=tmp_tail+strlen(syms[i].code);  //������� ���������� ��� �������� ������
                break;                         // ��������� �����
            }
        len++;                                  //������� �������
    }
    tail=(char)(tmp_tail%8);                    //�����
    rewind (fp_101);
    fclose (fp);

    newFname[j+1]='z';                          //������ ����������
    fp_zzz=fopen(newFname, "wb");
    //c������ ���������
    fprintf(fp_zzz, "%c%c%c%c", 'z','z','z',(char)(count-1)); //������������� � ���������� ���������� ��������
    for (i=0;i<count;i++){                      //������� ������/�������������
        fwrite(&syms[i].ch, sizeof(char), 1, fp_zzz);
        fwrite(&syms[i].freq, sizeof(syms[i].freq), 1, fp_zzz);
    }
    fwrite(&tail, sizeof(tail), 1, fp_zzz);
    fwrite(&len, sizeof(len), 1, fp_zzz);              //����� � ����� �����
    
    
    //���������� ������ ������
    while (fread(buf, sizeof(char), 8, fp_101))
        fputc(pack(buf), fp_zzz);
    
    fcloseall();
    return 0;
}