#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef unsigned short US;

struct COUNTRY
{
    char iso[3];
    US code;        
    char name[256];
    struct COUNTRY* head;
    struct COUNTRY* next;
};

typedef struct COUNTRY * PCOUNTRY;
typedef struct COUNTRY TCOUNTRY;

void AddList(PCOUNTRY country, char *line);
void FillCountry(PCOUNTRY country, char *line);
PCOUNTRY SearchISO(PCOUNTRY head, char ISO);
PCOUNTRY SearchNAME(PCOUNTRY head, char NAME);