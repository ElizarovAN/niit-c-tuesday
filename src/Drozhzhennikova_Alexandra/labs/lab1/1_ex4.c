#include <stdio.h>

void clean_stdin()
{
	int c;
	do{
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	int foot;
	int inch;
	float cm;

	while (1)
	{

		printf("Enter your heigh in feet and inches\n");
		if (scanf("%d%d", &foot, &inch) == 2)
		{
				if (inch <= 12 && inch > 0)
				{
					cm = (foot * 12 * 2.54f) + (inch * 2.54f);
					printf("Your heigh is %d feet and %d inches, that equels %f cantimeters\n", foot, inch, cm);
					break;
				}
				else
					printf("\nInput error!\n");
		}
		else
			clean_stdin();
			printf("\nInput error!\n");
	}
	return 0;
}