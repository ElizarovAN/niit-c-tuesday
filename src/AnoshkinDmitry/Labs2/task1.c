/*
. �������� ���������, ����������� ������ ���������� �����. ���-
�� ������ � ������ H, ������� �������� �������������. � �����
������ ������� ����� ������ ���������� ���������� �� �������
L =
gt2
2
, ��� g = 9.81 m
c
2
.
��������� ����� ����������� ��� � ������� � ������� �� �����-
��� ������� �������� ������ ��� ������������ ����� h.
*/

#include <time.h>
#include <stdio.h>

#define g 9.81f

int main()
{
    float H, H0;
    int t=0;
    clock_t now;

    puts ("Input Height, meters:");
    if(scanf ("%f", &H0)<1 || H0<0)
        puts ("Input Error");
    else
    {
        H=H0;
	    while(H>0)
	    {
	        printf ("t=%02d c, h=%06.1f m \n", t++, H);
            H=H0-g*t*t/2;
	        now=clock();
	        while(clock()<now+1000);
	     }
         puts("B-A-B-A-H");
     }
    return 0;
}