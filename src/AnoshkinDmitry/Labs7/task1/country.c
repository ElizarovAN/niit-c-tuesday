#include "country.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void AddList(PCOUNTRY country, char *line)
{
    if (!country)
    {
        country->head=(PCOUNTRY)malloc(sizeof(TCOUNTRY));
        country->next=NULL;
    }
    else if (country->next)
        AddList(country->next, line);
    else
    {
        country->next=(PCOUNTRY)malloc(sizeof(TCOUNTRY));
        country->next->next=NULL;
        FillCountry(country->next, line);
    }
}
            
void FillCountry(PCOUNTRY country, char *line)
{
    char buf[256];
    int i=1, j=1;
    while (*(line+i)!=',')
    {
        country->iso[i-j]=*(line+i);
        i++;
    }
    i++;
    country->iso[i-j]=0;
    j=i;
    while (*(line+i)!='"')
    {
        buf[i-j]=*(line+i);
        i++;
    }
    i++;
    buf[i-j]=0;
    country->code=atoi(buf);
    i++;
    j=i;
    while (*(line+i)!='"')
    {
        country->name[i-j]=*(line+i);
        i++;
    }
}

PCOUNTRY SearchISO(PCOUNTRY head, char ISO)
{
    return NULL;
}
PCOUNTRY SearchNAME(PCOUNTRY head, char NAME)
{
    return NULL;
}
