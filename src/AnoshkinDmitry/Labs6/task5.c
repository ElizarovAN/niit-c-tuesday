/*
�������� ���������, ������� �������� ����� ���������� N-���
����� ���� ���������. ������������� ����� ������� ��������
��� N � ��������� �� 1 �� 40 (��� � ������ ��������� �� �������)
�� ����� � � ����
���������:
��������� ���� �� ���������� ����� ������� � ����������� ������� � ��-
������� ������ ����������� ������� �� ����� ���� N
*/
//by Anoshkin Dmitry

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned long long ULL;

#define N 40

ULL fibonachi (int n)
{
    if (n==1||n==2)
        return 1;
    else
        return fibonachi(n-1)+fibonachi(n-2);
}
int main()
{
    FILE *fp;
    ULL fib;
    int i;
    clock_t begin, end;
    double delta=0;

    fp=fopen ("result.xls", "wt");
    for (i=1;i<=N;i++)
    {
        begin=clock();
        fib=fibonachi (i);
        end=clock();
        delta=((double)(end-begin)/CLOCKS_PER_SEC);
        fprintf(fp, "%d\t%.2lf\n", i, delta);
        printf("%d\t%.2lf\n", i, delta);
    }
    return 0;
}

