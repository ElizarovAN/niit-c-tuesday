/*
�������� ���������, ������� ��� �������� ������ ������� ����� �������
����� ��� �����
*/

//By Anoshkin D.M.

#include <stdio.h>
#include <string.h>

#define MaxLen 80
int main()
{
    char str[MaxLen]={0};
    char inWord=0;
    char i=0;
    unsigned char count=0, max=0, pstn=0, pstnMax=0;

    puts("Enter a line:");
    fgets(str, MaxLen, stdin);
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=' ';

    while (str[i])
    {
        if(str[i]!=' ')                 
        {
            if (inWord==0)
            {
                inWord=1;
                pstn=i;             //���������� ������� ������� ������� �����
            }
            count++;                //������� ����� �����
        }
        else if(str[i]==' ' && inWord==1)
        {
            if (count>max)
            {
                max=count;
                pstnMax=pstn;       //���������� ������� ������� ������� ������ �������� �����
            }
            count=0;
            inWord=0;
            pstn=0;
        }
        i++;
    }
    if (inWord==1)                  //���� ��� ������ ����� ��������� ������ ����� �����
    {
        if (count>max)
            {
                max=count;
                pstnMax=pstn;
            }
        count=0;
        inWord=0;
        pstn=0;
    }
    for(i=pstnMax; i<pstnMax+max; i++)  //����� ������
        putchar(str[i]);

    if (max)                            //����� ����� ������
        printf (" - %u \n", max);
    return 0;
}
