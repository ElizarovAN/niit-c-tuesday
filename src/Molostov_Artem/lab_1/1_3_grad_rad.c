/*
�������� ���������, ������� ��������� �������� ���� �� �����-
��� � �������, �, ��������, � ����������� �� ������� ��� �����.
��������: 45.00D - �������� �������� � ��������, � 45.00R - �
��������. ���� ������ �������������� �� ������� %f%c
*/

#include <stdio.h>

void clean_stdin()
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c !=EOF);
}

int main()
{
   float value,translate;
   char what;
   while(1) // �������� ������������ ����� ������
   {
       puts("Enter a value of angle degree or radian [format: 45.00D or 45.00R]");
        if ((scanf("%f%c",&value,&what)==2) && (what=='D' || what=='R' || what=='d' || what=='r'))
                break; 
            else 
            puts("Input error");
            clean_stdin();
   }
   if (what=='r' || what=='R') // �������� ��� ��� ������� ��� �������
   {
       translate=value*57.3; // ������� ������ � �������
       printf("Your radian %f = %.2f degree\n",value,translate); // ����� ���������� 
   }
   else
   {
       translate=value*0.017; // ������� �������� � �������
       printf("Your degree %f = %.2f radian\n",value,translate); // ����� ���������� 
   }
 return 0;
}