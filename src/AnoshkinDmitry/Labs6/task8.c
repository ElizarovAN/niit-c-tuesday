/*
8. �������� ���������, ������� ��������� ������������� ���������
��������������� ���������, ��������� � ���� ��������� ������-
��� ������. ������������� ��������� 4-� �������� ��������. ��-
����� ���������� ������������ �������� ��������
���������:
� ������ ��������� ����� ����������� �������: 0-9,+,-,*,/,(,) ��������� ��-
��� ���� ��������, �� ���� ������� �������� �� ������ ����� 3, 8, � �����
���� ��������, ��������, ((6+8)*3) ��� (((7-1)/(4+2))-9). ������������-
��, ��� ������ � ��������� ������ ���������, �� ���� ���������� ��������
����� ���������� �������� � ��� �� ���������� ��������.
��������� ������ �������� �� ��������� �������:
(a) int main(int argc, char* argv[]) - ������� �������, � ������� ����������-
���� ����� ����������� ������� eval ��� ���������� ���������
(b) int eval(char *buf) - �������, ����������� ������, ������������ � buf
(c) char partition(char *buf, char *expr1, char *expr2) - �������, ������� ���-
������ ������, ������������ � buf �� ��� �����: ������ � ������ ���-
������, ���� �������� � ������ �� ������ ���������
*/
//by Anoshkin D.M.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define MaxLen 256

int checkString(char *str);
char Select(char *str1, char *str2);
int Calc(char *str);

int main(int argc, char *argv[])
{
    char str_in[MaxLen];
    if (argc<2){
        puts("Input Error");
        return 1;
    }
    strcpy(str_in, *(argv+1));
    if(checkString(str_in)==0)
    {
        puts("String Error");
        exit(0);
    }
    printf("%s = %d\n", str_in, Calc (str_in));
    return 0;
}

char Select(char *str1, char *str2)
{
    //�������� ������ � ������� (�+�)
    //���������� ��� ������ �, +, �
    char tmp[MaxLen], math;
    int pstn=1, brackets=0, i;
    strcpy(tmp, str1);
    do 
    {
        if(tmp[pstn]=='(')
            brackets++;
        if(tmp[pstn]==')')
            brackets--;
        pstn++;
    }
    while (brackets>0);
    while (tmp[pstn]>='0')
        pstn++;
    math=tmp[pstn];
    for (i=1;i<pstn;i++)
        str1[i-1]=tmp[i];
    str1[i-1]=0;
    for (i=pstn+1; i<strlen(tmp)-1;i++)
        str2[i-pstn-1]=tmp[i];
    str2[i-pstn-1]=0;
      
    return math;
}

int Calc(char *str)
{
    char str1[MaxLen], str2[MaxLen];
    if(str[0]!='(')
        return atoi(str);
    strcpy(str1, str);
    switch (Select(str1, str2)){
        case '+':
            return Calc(str1)+Calc(str2);
            break;
        case '-':
            return Calc(str1)-Calc(str2);
            break;
        case '*': 
            return Calc(str1)*Calc(str2);
            break;
        case '/':
            return Calc(str1)/Calc(str2);
            break;
        default:
            if(str1[0]>='0' && str1[0]<='9')
                return atoi(str1);
            else
            {
                puts ("Wrong math");
                exit(0);
            }
    }
    puts("Unknown Error!");
    exit(0);
    return 0;
}

int checkString(char * str) //������� ������ - ��������� ��������� �� ���� (�+�)
{
    char tmp[MaxLen];
    int pstn, i, j;
    int MathCount=0;
    
    strcpy(tmp, str);
    pstn=strlen(tmp)-1;
    if(tmp[0]!='(')     //�������� �������� �� ������ ������ ������
    {
        for(j=0;j<strlen(tmp);j++)
            if (tmp[j]<'0' || tmp[j]>'9')
                return 0;
        return 1;
    }
    if (tmp[0]!='(' || tmp[pstn]!=')')  //���� ��� ������ � ������ � ����� ������
        return 0;
    i=pstn;
    while(tmp[i]!='(' && i>=0)          //���� ������ ������ ������� ������
    {
        if(tmp[i]==')')
            pstn=i;
        i--;
    }
    if (i==0 && pstn!=strlen(tmp)-1)    //���� �� ����� ����������� ������
        return 0;
    for (j=i+1;j<pstn;j++)              //���� ������ ��� ��� ��������
        if(tmp[j]<'0')
            MathCount++;
    if (MathCount>1)
        return 0;
    if (i==0 && pstn==strlen(tmp)-1)    //���� ��� ��������� ���� (�+�)
        return 1;
    else
    {
        tmp[i]='0';                     //���� ����� ������� �������� ������ � �� ���������� �� "0"
        j=i+1;
        while(tmp[j+pstn-i]){           
            tmp[j]=tmp[j+pstn-i];
            j++;
        }
        tmp[j]=0;
        return checkString(tmp);       //������������ ��� ���
    }
}