/*
�������� ��������� ��� ������ ����� ������� ������������������� � ���-
���� � �������������� ���������� ������ �������� ����������.
���������:
���������� ����� �������� ������ ���� �������� � �������������� ��������
���, ����� ������ � ������ ������������� ����� ���������.
����� �� ������������������ ���� "���� ���� ���" = "����"
*/

//by Dmitry Anoshkin

#include <stdio.h>

#define MaxLen 100

int main()
{
    char str[MaxLen]={0};
    char *start=str;
    char i=0, j=0;
    char max=0, current=0;
    puts ("Input string: ");
    fgets(&str, MaxLen, stdin);
    while (str[i] && str[i]!=10)
    {
        j=i;
        while (str[i]==str[j++])
            current++;
        if (current>max)
        {
            max=current;
            start=&str[i];
        }
        i=i+current;
        current=0;
    }
    printf ("%d - ", max);
    for (i=0; i<max; i++)
        putchar(*(start+i));
    putchar('\n');
    return 0;
}
