/*
10. �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� �� ������. �
������ ������������� n ��������� ��������� �� ������
���������:
� ���������� ������ ��������� ������� ������ ���� ������� �� �������.
��������� �������������� ������� ������
*/
// by Anoshkin Dmitry

#include <stdio.h>

#define MaxLen 100

int main()
{
    char str[MaxLen]={0};
    int n;
    char i=0, j=0;
    char wordCount=0, Len=0;
    char del=0;

    puts("Input string: ");
    fgets(&str, MaxLen, stdin);
    puts("Input word number:");
    scanf("%d", &n);

    while (str[i] && str[i]!=10)
    {
        while (str[i]==' ') i++;
        wordCount++;
        j=i;
        while(str[j]!=' ' && str[j]&& str[j]!=10)
                j++;
            
        if (n==wordCount)
        {
            if (str[j]==' ')
                j++;
            do
            {
                str[i]=str[j];
                i++;
                j++;
            }
            while (str[j-1]!=0 && j<MaxLen);
            del=1;
            break;
        }
        else
            i=j;
    }
    if (del)
        printf("%s", str);
    else
        puts("Number is too big!");
    return 0;
}