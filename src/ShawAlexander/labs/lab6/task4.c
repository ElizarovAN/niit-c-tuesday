/*4. �������� ���������, ������� ��������� ������ ������������ �
����������� ���������
���������:
��������� ��������� ��������� ������������������ ��������:
(a) ��������� �� ��������� ������ �������� ������� ������
M
;
(b) ������� ������ ������������� �������
N = 2^m
;
(c) �������� ������ ��� ������������ ������;
(d) ��������� ������� ��������� ������ �������;
(e) ������� ����� ������������ � ������������ ��������;
(f) ���������� ����� ���������� ������������ ������������ � ��������-
��� ��������;
(g) ����������� ������������ ������
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
time_t getTime();
unsigned int power(int, int);
long sumArr(int*, int);
int* rewindPointer(int* ptr, int n);

int main() {
    int *p_array = 0;
    int pwr = 0;
    puts("Set an amount of memory (>=2) to use (the greater number you choose, the more memory will be allocated):");
    if (scanf("%d", &pwr) != 1 || pwr < 2) { //(a) - ������ ������� ������
        puts("Input error!");
        return -1;
    }
    unsigned int memsize = power(2, pwr); //(b) - ������� ������ �������
    p_array = malloc(memsize); //(c) - �������� ������ ��� ������
    printf("Memory = %d bytes\n", memsize);
    srand(time(0));

    clock_t start = clock(), end;
    //(d) - ����������
    for (int i = 0; i < memsize; i += 4) {
        *p_array = rand() % 100 + 1;
        //printf("%d\n", *p_array);
        if (i + 1 != memsize)
            p_array++;
    }
    p_array = rewindPointer(p_array, memsize);

    //(e) - ������������ ������
    start = clock();
    long sum = 0;
    for (int i = 0; i < memsize; i += 4) {
        sum += *p_array;
        if (i + 1 != memsize)
            p_array++;
    }
    end = clock();
    double t = (double)(end - start) / CLOCKS_PER_SEC;
    p_array = rewindPointer(p_array, memsize);

    //(e) - ����������� ������
    start = clock();
    sum = sumArr(p_array, memsize / 4);
    end = clock();
    double trec = (double)(end - start) / CLOCKS_PER_SEC;

    //(f) - ��������� ������� ���������� ������� ���������
    printf("----------\nNormal time: %.3lfsec\nRecursion time: %.3lfsec\n----------\n%s\n", t, trec,
        t < trec ? "Normal way is faster." : (t == trec ? "Both ways have equal time." : "Recursion way is faster."));
    //(g) - ������������ ������
    free(p_array);
    return 0;
}
//��������� �������� �������
time_t getTime() {
    time_t seconds = 0;
    time(&seconds);
    return seconds;
}
//���������� ����� n � ������� p
unsigned int power(int n, int p) {
    if (p == 0)
        return 1;
    else if (p == 1)
        return n;
    else if (p < 0)
        return 0;
    else {
        return n*power(n, p - 1);
    }
}
//����������� ������� ��� ���������� ����� ��������� ������� int
long sumArr(int *arr, unsigned int size) {
    if (size == 1)
        return *arr;
    else {
        return sumArr(arr, size / 2) + sumArr(arr + size / 2, size - size / 2);
    }
}
//������� ���������� ������ int, ����������� �� n ��������� �����
int* rewindPointer(int* ptr, int n) {
    return ptr -= (n / 4);
}
