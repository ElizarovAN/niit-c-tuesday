#include "Task1.h"

PITEM createList(PCOUNTRY country)
{
	PITEM item=(PITEM)malloc(sizeof(TITEM));
	item->country=country;
	item->prev=NULL;
	item->next=NULL;
	return item;
}
PCOUNTRY createName(char *line)
{
	int i=0;
	PCOUNTRY rec=(PCOUNTRY)malloc(sizeof(TCOUNTRY));
	while(*line && *line!=',')
		rec->iso[i++]=*line++;
	rec->iso[i]=0;
	line++;
	i=0;
	while(*line && *line!=',')
		rec->code=*line++;
	*line++;
	*line++;
	while(*line && *line!='"')
		rec->name[i++]=*line++;
	rec->name[i]=0;
	return rec;
}
PITEM addToTail(PITEM tail, PCOUNTRY country)
{
	PITEM item=createList(country);
	if(tail!=NULL)
	{
		tail->next=item;
		tail->prev=tail;
	}
	return item;
}
int countList(PITEM head)
{
	int count=0;
	while(head)
	{
		count++;
		head=head->next;
	}
	return count;
}
PITEM findByName(PITEM head, char *name)
{
	while(head)
	{
		if(strcmpi(head->country->name,name)==0)
			return head;
		head=head->next;
	}
	return NULL;
}
PITEM findByISO(PITEM head, char *iso)
{
	while(head)
	{
		if(strcmpi(head->country->iso,iso)==0)
			printISO(head);
		head=head->next;
	}
	return NULL;
}
void printISO(PITEM item)
{
	if(item!=NULL)
	{
		printf("%d",item->country->code);
		printf(" %s\n",item->country->name);
	}
}
void printName(PITEM item)
{
	if(item!=NULL)
	{
		printf("%d",item->country->code);
		printf(" %s\n",item->country->iso);
	}
}