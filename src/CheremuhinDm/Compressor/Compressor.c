// Компрессор Хаффмана.
// Автор работы: Черёмухин Дмитрий
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 10000000 // Величина буфера 101

typedef unsigned char uc;

struct SYM {
	short ch;
	unsigned int freq;
	char code[256];
	struct SYM *left;
	struct SYM *right;
};

typedef struct SYM TSYM;
typedef TSYM * PSYM;

PSYM createTree(PSYM node, short c, short*p_number) { // Создаёт дерево статистики.
	if (node == NULL) {
		node = (PSYM)calloc(1,sizeof(TSYM));
		node->ch = c;
		node->freq++;
		(*p_number)++;
		node->left=node->right=NULL;
	}	
	else if (node != NULL && node->ch == c) node->freq++;
		else if (node != NULL && node->ch - c > 0) node->left = createTree(node->left, c, p_number);
			else if (node != NULL && node->ch - c < 0) node->right = createTree(node->right, c, p_number);					
	return node;
}

PSYM createArray(PSYM node, PSYM arr, short*i) { // Создаёт массив из дерева статистики
	if(node->left) createArray(node->left, arr, i);	
	(arr[(*i)-1]) = (*node);
	(*i)--;
	if(node->right) createArray(node->right, arr, i);
	return arr;
}

int comp(const void*b, const void*a) { // Компаратор
	return (((PSYM)a)->freq - ((PSYM)b)->freq);	
}

PSYM makeTable(FILE*fp, PSYM tmp, short*c) { // Создаёт таблицу встречаемости на основе дерева статистики.
	short n = 0;								// Сортирует по убыванию.
	int size = 10;
	PSYM Arr = NULL;
	if(!fp) {
		perror("ERROR");
		getch();
		exit(1);
	}
	fseek( fp, 0, SEEK_END); 
	size = (ftell(fp)); 
	if (size == 0) {
		puts("There is no content in the source file!");
		exit(2);
	}
	rewind(fp);
	while(!feof(fp)) tmp = createTree(tmp, fgetc(fp), &n);
	Arr = createArray(tmp, ((PSYM)malloc(((*c) = n)* sizeof(TSYM))), &n);
	qsort(Arr, (*c), sizeof(TSYM), comp);
	
	return Arr;
}

PSYM* ppArr(PSYM tmp, short c) { 					// Переводит таблицу встречаемости в массив указателей на указатели на структуру, 
	int i = 0;										// для передачи в ф-ю построения H-дерева
	PSYM* pp = (PSYM*)calloc(c+1, sizeof(PSYM));	
	for(i=0; i<c; i++){		
		pp[i] = &tmp[i];
		pp[i]->left=pp[i]->right=NULL;
	}
	return pp;
}

PSYM buildTree(PSYM psym[], int s) {	// Построитель H-дерева
	short i = 0, n;
	PSYM tmp = NULL;
	struct SYM *temp=(struct SYM*)malloc(sizeof(struct SYM));
	temp->ch = -5;
	temp->freq=psym[s-2]->freq+psym[s-1]->freq;
	temp->left=psym[s-1];
	temp->right=psym[s-2];
	temp->code[0]=0;
	if(s==2) 
	return temp;
	psym[s-1] = NULL;
	psym[s-2] = temp;
	n = s - 2;
 	for(i=0;i<n && psym[n-i]->freq >= psym[n-i-1]->freq;i++){
		tmp = psym[n-i];
		psym[n-i] = psym[n-i-1];
		psym[n-i-1] = tmp;		
	}
	return buildTree(psym,s-1);
}

void makeCodes(PSYM root) {		// Кодер
	if(root->left != NULL) {
		strcpy(root->left->code,root->code);
		strcat(root->left->code,"0");
		makeCodes(root->left);
	}
	if(root->right != NULL) {
		strcpy(root->right->code,root->code);
		strcat(root->right->code,"1");
		makeCodes(root->right);
	}
}

void titleMaker(short c, PSYM buf, char* fname) {	// Функция создаёт заголовок сжимаемого файла.
	short i = 0;
	int s = 0;
	char out[50] = {0};
	FILE*fp_OUT = fopen(strcat( strcat( out, fname), ".comp"), "wb");
	for( i = 0; i < c; i++) s += buf[i].freq;
   	fprintf(fp_OUT, "%s%c", "dstc", 0);   	
	fwrite(&s, sizeof(int), 1, fp_OUT);
	fwrite(&c, sizeof(short), 1, fp_OUT);
	fflush(fp_OUT);
   	fp_OUT = fopen(out, "ab");
	for( i = 0; i < c; i++){
 		fwrite(&buf[i].ch, sizeof(uc),1,fp_OUT);
		fwrite(&buf[i].freq, sizeof(int),1,fp_OUT);
	}
	fclose(fp_OUT);
}

unsigned char makeByte(const char* data) {	// Сборщик байтов.
   unsigned char byte = 0;
   const char* end = data + 8; 
   for (; *data && (data < end); ++data) {
      byte <<= 1; 
      byte |= (*data == '1') ? 1 : 0;
   } 
   return byte;
}

void Compressor( char* fname, PSYM tmp, short c) {	// Собственно компрессор
	int i = 0, j = 0, count = 0;
	char out[50] = {0};	
	short tempr = 0;
	FILE*fp_IN = fopen( fname, "rb");
	FILE*fp_OUT = fopen(strcat( strcat( out, fname), ".comp"), "ab");	
	char*pArr = ( char*)calloc( N ,sizeof( char));
	while(1) { 
		if( (tempr = fgetc(fp_IN)) == EOF) 
			break;
		for ( i = 0; i < c; i++) 
			if (tmp[i].ch == tempr) 
				break;
		for(j = 0; tmp[i].code[j]; j++)
			pArr[count+j] = tmp[i].code[j];
		count+=j;
		if ( count%8 == 0) {
			for(i = 0; i < strlen(pArr)-1; i += 8) {
				tempr = makeByte( &pArr[i]);
				fwrite( &tempr, sizeof(uc), 1, fp_OUT);
			}
			for( ; i >= 0; i--) pArr[i] = 0;
			count = 0;				
		}		
	}	
	if( strlen(pArr) != 0) {
		count = 0;
		while( strlen(pArr)%8 != 0)	{
			pArr = strcat( pArr, "0");
			count++;
		}
		for(i = 0; i < strlen(pArr)-1; i += 8) {
			tempr = makeByte( &pArr[i]);		
			fwrite( &tempr, sizeof(uc), 1, fp_OUT);
		}
	}
	fflush(fp_OUT);
	fp_OUT = fopen(out, "r+b");	
	fseek( fp_OUT, 4, SEEK_SET);	// <<<Возврат каретки
	fputc( (char)count, fp_OUT); 	// <<<Запись величины хвоста
	fclose(fp_IN);
	fclose(fp_OUT);
}

int main(int argc, char* argv[]) {
	clock_t t1 = clock();	
	PSYM tmp = NULL, buf = NULL;
	FILE*fp = NULL;	
	short c = -1;
	if (!argv[1]) {
		printf("\nHello, dear user! This short manual is for you!\n\n"	
		"To use this compressor you should enter the following commands:\n"
		"<disk>:/<dir>/compressor.exe <disk>:/<dir>/<fname.ext>\n\n"
		"Complaints and suggestions: d.st.cheremuhin@gmail.com\n\nPress any key.\n\n");
		getch();
		exit (1);
	}
	tmp = makeTable(fopen(argv[1], "rb"), tmp, &c); 
	buf = buildTree(ppArr(tmp, c), c);
	makeCodes(buf);			
	titleMaker(c, tmp, argv[1]);
	Compressor(argv[1], tmp, c);	
	printf("\nInput file: %s\nOutput file: %s.comp\nCompression completed.\nElapsed time: %.3f\n", 
		argv[1], argv[1], ((double)(clock()-t1)/CLOCKS_PER_SEC));
	return 0;
}
