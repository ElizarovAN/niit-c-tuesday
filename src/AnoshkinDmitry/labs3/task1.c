/*
�������� ���������, �������������� ���������� ���� �� �����-
��� ������������� ������
*/

//By Anoshkin D.M.

#include <stdio.h>
#include <string.h>

#define MaxLen 80
int main()
{
    char str[MaxLen];
    char inWord=0;
    char i=0;
    unsigned char count=0;

    puts("Enter a line:");
    fgets(str, MaxLen, stdin);
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=' ';


    while (str[i])
    {
        if(str[i]!=' ' && inWord==0)
        {
            inWord=1; 
            count++;
        }
        else if(str[i]==' ' && inWord==1)
            inWord=0;
        i++;
    }
    if (inWord==1)                      // �������� �� �� ��� ������ �� ���������
        inWord=0;
    printf("Word number is %u\n", count);
    return 0;
}
