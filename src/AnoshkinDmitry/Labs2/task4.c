/*
�������� ���������, �������������� ������� � ������� �������� �������:
������� ���� ��������� �����, ����� �����. ������ �������� � ���� �������-
�� � ���� ��������� ������������������ ���� � ����. ������������ �������-
�������� ��������� ������.
���������:
���������� � ������ ������ ����������� ����� �� ������������. ����� ��-
���������� ����������� ��������� �������.
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>


#define MaxLen 20

int main()
{
    char str[MaxLen] = {0};
    int choise;                //letters or numbers
    int i=0, j=0;
    char tmp;

    srand(time(0));             //����� ��������������� ����� ���� ���������� (����� ������������������)
    
    for (i=0; i<MaxLen-1; i++)
    {
        choise=rand()%2;
        if (choise)
            str [i] = rand()%('9'-'0'+1)+'0';     //numbers
        else
            str [i] = rand()%('z'-'a'+1)+'a';     //letters
    }
    printf ("Source string: \n %s \n", str);
    
    i=0;    
    j=MaxLen-1;
    while (i<j)
    {
        if (str[i]<='9')
            if (str [j]>='a')
            {
                tmp=str [i];
                str[i]=str[j];
                str[j]=tmp;
            }
            else
                j--;
        else
            i++;
    }
    printf ("Result string: \n %s \n", str);
    return 0;

}