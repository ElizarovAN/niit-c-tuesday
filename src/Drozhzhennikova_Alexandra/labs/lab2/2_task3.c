#include <stdio.h>


int main()
{
	int count;
	int n = 40;
	int line = 1;
	int star = 1;

	printf("enter the number of lines: \n");
	scanf("%d", &count);
	for (int b = 0; b <= count; b++)
	{
		for (int i = 0; i <n - star; i++)
		{
			printf(" ");
		}
		for (int a = 0; a < line; a++)
		{
			printf("*");
		}
		line = line + 2;
		star++;
		printf("\n");
	}
	return 0;
}