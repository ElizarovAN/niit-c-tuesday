#include <stdio.h>
#include <string.h>


typedef struct SYM TSYM;
typedef TSYM *PSYM;
PSYM psym[256] = { 0 };

char path_buffer[_MAX_PATH];
char drive[_MAX_DRIVE];
char dir[_MAX_DIR];
char fname[_MAX_FNAME];
char ext[_MAX_EXT];

typedef struct SYM
{
    char symbol;
    float freq;
    char code[256]; //������ ��� ������ ����
    struct SYM *left; //����� �������
    struct SYM *right; //������ �������
}SYM;

int countStruct(FILE *infile)
{
    char syms[256] = { 0 };
    char ch;
    int count = 0, i = 0, total = 0;
    while ((ch = fgetc(infile)) != EOF)
    {
        if (ch!=' '&& ch!='\n')
        {
            if (!syms[ch])
            {
                syms[ch] = 1;
                count++;
            }
            else
                syms[ch]++;
            total++;
        }
        else
            continue;
    }

    return count;
}


struct SYM* buildTree(int N)
{
    // ������ ��������� ����
    PSYM temp = (struct SYM*)malloc(sizeof(struct SYM));
    // � ���� ������� ������������ ����� ������
    // ���������� � �������������� ��������� ������� psym
    temp->freq = psym[N - 2]->freq + psym[N - 1]->freq;
    // ��������� ��������� ���� � ����� ���������� ������
    temp->left = psym[N - 1];
    temp->right = psym[N - 2];
    temp->code[0] = 0;
    if (N == 2)
        return temp;
    // ��������� temp � ������ ������� psym,
    // �������� ������� �������� �������
    int count = N - 1;
    for (int i = 0; i < N; i++)
    {
        if (psym[i]->freq < temp->freq)
        {
            if (count>2)
            {
                for (int a = count - 2; a >= i; a--)
                {
                    psym[a + 1] = psym[a];
                }
                psym[i] = temp;
                break;
            }
            else
            {
                if (count == 2)
                {
                    psym[i + 1] = psym[i];
                    psym[i] = temp;
                    break;
                }
            }
        }
        else
            continue;
    }
    return buildTree(N - 1);
}

void makeCodes(PSYM root)
{
    
    if (root->left)
    {
        strcpy(root->left->code, root->code);
        strcat(root->left->code, "0");
        makeCodes(root->left);
    }
    if (root->right)
    {
        strcpy(root->right->code, root->code);
        strcat(root->right->code, "1");
        makeCodes(root->right);
    }
 
}

typedef union CODE
{
    unsigned char ch;
    struct 
    {
        unsigned short b1 : 1;
        unsigned short b2 : 1;
        unsigned short b3 : 1;
        unsigned short b4 : 1;
        unsigned short b5 : 1;
        unsigned short b6 : 1;
        unsigned short b7 : 1;
        unsigned short b8 : 1;
    }byte;
}CODE;

char pack(unsigned char *arr) 
{
    CODE code;
    code.byte.b1 = arr[0] - '0';
    code.byte.b2 = arr[1] - '0';
    code.byte.b3 = arr[2] - '0';
    code.byte.b4 = arr[3] - '0';
    code.byte.b5 = arr[4] - '0';
    code.byte.b6 = arr[5] - '0';
    code.byte.b7 = arr[6] - '0';
    code.byte.b8 = arr[7] - '0';
    return code.ch;
}


void  unpack(unsigned char *buf)
{
    union CODE code;
    code.ch = buf[0];
    buf[0] = code.byte.b1 + '0';
    buf[1] = code.byte.b2 + '0';
    buf[2] = code.byte.b3 + '0';
    buf[3] = code.byte.b4 + '0';
    buf[4] = code.byte.b5 + '0';
    buf[5] = code.byte.b6 + '0';
    buf[6] = code.byte.b7 + '0';
    buf[7] = code.byte.b8 + '0';
}
int tail_length(FILE *infile, PSYM sym, int i)
{
    rewind(infile);
    char mas[9] = { 0 };
    char c = 0, a = 0, b = 0;
    while ((c = fgetc(infile)) != -1 && c != '\n')
    {
        int count = 0;
        for (a = 0; a <= i; a++)
        {
            if (sym[a].symbol == (unsigned char)c)
            {
                for (count = 0; count < strlen(sym[a].code); count++)
                {
                    mas[b++] = sym[a].code[count];
                    if (b == 8)
                    {
                        for (b = 0; b < 8; b++)
                            mas[b] = 0;
                        b = 0;
                    }
                }
                break;
            }
        }
    }
    if (b<8)
        return b;
    else
        return 0;
}

void print(TSYM *sym, int count_struct)
{
    for (int i = 0; i <= count_struct; i++)
    {
        printf("symbol %c repits %.3fpersent\n", sym[i].symbol,
            sym[i].freq);
    }
}

int cmp(int count_struct)
{
    int i = count_struct;
    int b = i - 1, a = count_struct;
    PSYM temp = 0;
    for (i, b; i >= 1, b >= 0; i--, b--)
    {
        if (psym[i] != NULL)
        {
            if (psym[i]->freq > psym[b]->freq)
            {
                temp = psym[b];
                psym[b] = psym[i];
                psym[i] = temp;
                a = b;
                for (a; a< count_struct - 1; a++)
                {
                    if (psym[a]->freq < psym[a + 1]->freq)
                    {
                        temp = psym[a];
                        psym[a] = psym[a + 1];
                        psym[a + 1] = temp;
                    }
                    else
                        continue;
                    a++;
                }
            }
            else
                continue;
        }
        else
            continue;
    }
}
void searchForSymbol(FILE *temp, FILE *result, PSYM root, int count_symbol)
{
    PSYM node = root;
    int a = 0;
    while (a!= -1)
    {
        root = node;

        while (count_symbol > 0)
            {
                a = fgetc(temp);
                if (a == 49)
                {
                    
                    if (root->right)
                       root = root->right; 
                        
                                            
                    if (!root->right)
                    {
                        count_symbol--;
                        fputc(root->symbol, result);
                        printf("%c", root->symbol);
                        break;                        
                    }
                        
                }

                else if (a == 48)
                {
                  
                    if (root->left)
                          root = root->left;
                    
                    if (!root->left)
                    {
                        count_symbol--;
                        fputc(root->symbol, result);
                        printf("%c", root->symbol);
                        break;
                    }
                }
          
            }        
    }    
}
    
    
   
