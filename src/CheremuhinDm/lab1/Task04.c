/*��������� ����������� ���� � ����� � ����������*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int main()
{
	int foot = 0, inch = 0;
	float coeff = 2.54f, result = .0f;

	puts("Please, enter your value.\nUse space to define foots and inches.\n\nFor example: 12'34\n\n");
	scanf("%d'%d", &foot, &inch);
	if (foot >= 0 && inch >= 0)
	{
		result = ((foot * 12 + inch)*coeff);
		printf("Your value is: %.1f cm!\n", result);
	}
	else 
		puts("Input error!");
	return 0;
}