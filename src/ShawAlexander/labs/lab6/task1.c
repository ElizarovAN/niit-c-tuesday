#include <stdio.h>
#include <Windows.h>
#define SIZE 81
void printArr(char (*)[SIZE]);
unsigned int power(unsigned int, int);
void buildFractal(char(*field)[SIZE], int y, int x, int width, static int stopN);
void getFractal(char(*field)[SIZE], int iterations);
void fillArr(char(*field)[SIZE], char);
 
int main() {
    char fractal[SIZE][SIZE] = { 0 };
    fillArr(fractal, ' ');
    //�������� �� ������ ���������
    for (int i = 0; i < 5; i++)
    {
        system("cls");
        getFractal(fractal, i);
        printArr(fractal);
        fillArr(fractal, '\0');
        Sleep(1000);
    }
    return 0;
}
//����� ������� �� �����
void printArr(char(*field)[SIZE]) {
    for (int i = 0; i < SIZE; i++)
    {
        for (int q = 0; q < SIZE; q++) {
            putchar(field[i][q]);
        }
        putchar('\n');
    }
}
//���������� ������� �������� ��������
void fillArr(char(*field)[SIZE], char c) {
    for (int i = 0; i < SIZE; i++)
    {
        for (int q = 0; q < SIZE; q++) {
            field[i][q] = c;
        }
    }
}
//���������� ��������
void buildFractal(char(*field)[SIZE], int y, int x, int width, static int stopN) {
    if (stopN >= 0) {
        stopN--;
        buildFractal(field, y + power(3, stopN), x + power(3, stopN), power(3, stopN), stopN);
        if (x == SIZE-power(3, stopN+1) && y == SIZE - power(3, stopN+1))
            return;
        //������������ �������� �����
        for (int leftY = y, i = 0; i < width; i++, leftY++) {
            for (int leftX = x - width, q = 0; q < width; q++, leftX++) {
                field[leftY][leftX] = field[y + i][x + q];
            }
        }
        //������������ �������� ������
        for (int rightY = y, i = 0; i < width; i++, rightY++) {
            for (int rightX = x + width, q = 0; q < width; q++, rightX++) {
                field[rightY][rightX] = field[y + i][x + q];
            }
        }
        //������������ �������� ������
        for (int upperY = y - width, i = 0; i < width; i++, upperY++) {
            for (int upperX = x, q = 0; q < width; q++, upperX++) {
                field[upperY][upperX] = field[y + i][x + q];
            }
        }
        //������������ �������� �����
        for (int lowerY = y + width, i = 0; i < width; i++, lowerY++) {
            for (int lowerX = x, q = 0; q < width; q++, lowerX++) {
                field[lowerY][lowerX] = field[y + i][x + q];
            }
        }
    }
    else {
        field[y-1][x-1] = '*';
    }
}
//������� ��� buildFractal()
void getFractal(char(*field)[SIZE], int iterations) {
    if (iterations >= 0)
        buildFractal(field, SIZE - power(3, iterations), SIZE - power(3, iterations), power(3, iterations), iterations);
}
//���������� 3 � ������� p
unsigned int power(unsigned int number, int p) {
    if (p == 0 || p < 0)
        return 1;
    else {
        for (int i = 1; i < p; i++) {
            number *=3 ;
        }
    }
    return number;
}