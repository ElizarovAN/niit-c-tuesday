#include <stdio.h>
#include <time.h>

typedef unsigned long long ULL;

ULL sum(ULL *mas, ULL N)
{
    if (N == 1)
        return *mas;
    else
        return sum(mas, N/2) + sum(mas+N/2, N - N/2);
}
ULL power(ULL x, ULL y)
{
    if (y == 1)
        return x;
    else
        return x=x*power(x, y - 1);
}

int main(int argc, char *argv[])
{
        srand(time(0));
        ULL M = 0, N = 0, summa;
        int i;
        if (argc != 2)
        {
            printf("Wrong number!\n");
            exit(1);
        }
        else
            M = atoi(argv[2]);

    N = power(2, M);
    ULL  *mas = (ULL *)calloc(N, sizeof(ULL));
    if (!mas)
    {
        puts("Memory allocation error!");
        exit(1);
    }

    for (int i = 0; i < N; i++)
    {
        mas[i]= rand() % N;
        printf("%d  ", mas[i]);
    }
    summa = sum(mas, N);
        free(mas);

    return 0;
}