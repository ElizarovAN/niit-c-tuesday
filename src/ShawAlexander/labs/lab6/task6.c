/*6. �������� ���������� ����������� �������, ����������� n-��
������� ���� ���������, �� ��� ��������������� �������� �����-
���
���������:
����� ������� ��� �������: ���� ���������� ��������������� �� main � ��-
������ ������, ���������������, ������� � �������� �����������.
*/
#include <stdio.h>
typedef unsigned long long llu;
llu fib(llu, llu, int);
llu getFib(int);
int main() {
    printf("%llu\n", getFib(40));
    return 0;
}
llu fib(llu a, llu b, int n) {
    if (n == 0)
        return a;
    else
        return fib(a + b, a, n - 1);
}
llu getFib(int n) {
    return fib(1, 0, n);
}