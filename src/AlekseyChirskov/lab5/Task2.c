/*�������� ��������� "�����������", ��������� �� ����� ������-
�����, ������������ �� ����������� ������������� ��������� '*'.
����������� ����������� � ��������� ���������� �������, � ��-
��� ��� ����� � ����������� ���������� � ��������� ��� �����.
���������:
������� ������ ��������� � ���� ��������� ������������������ �����:
1) ������� ������� (���������� ���������)
2) ������������ ��������� ������� �������� ������ ��������� (��������� '*')
3) ����������� �������� � ������ ��������� �������
4) ������� ������
5) ����� ������� �� ����� (���������)
6) ��������� ��������
7) ������� � ���� 1.*/
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#define CLOCKS_PER_SEC 1000//���� ����� �� 1000
#define N 20//���-�� �����
#define M 30//����� ������
#define SEC 1//�������� � ��������
void randomMas(char **pmas)//������������ ��������� ������� �������� ������ ��������� (��������� '*')
{
	int i,j,value,random=1;
	srand (time(NULL));
	random=rand()%3+4;
	for(i=0;i<N/2;i++)
	{
		for(j=0;j<M/2;j++)
		{
			value=rand()%random;
			if(value==0)
				pmas[i][j]='*';
			else pmas[i][j]=' ';	
		}
	}
}
void randomOthers(char **pmas)//����������� �������� � ������ ��������� �������
{
	int i,j,end_N,end_M;
	end_N=N-1;
	for(i=0;i<N/2;i++)
	{
		end_M=M-1;
		for(j=0;j<M/2;j++)
		{
			pmas[i][end_M]=pmas[i][j];
			pmas[end_N][j]=pmas[i][j];
			pmas[end_N][end_M]=pmas[i][j];
			end_M--;
		}
		end_N--;
	}
}
void printAll(char **pmas)//����� ������� �� ����� (���������)
{
	int i,j;
	for(i=0;i<N;i++)
	{
		for(j=0;j<M;j++)
			printf("%c",pmas[i][j]);
		printf("\n");
	}
}
int main()
{
	int i,j;
	char mas[N][M]={0},*pmas[N]={0};
	clock_t start;
	for(i=0;i<N;i++)
		pmas[i]=mas[i];
	while(1)
	{
		clock_t delay=SEC*CLOCKS_PER_SEC;
		memset(mas,' ',sizeof(mas));//������� ������� (���������� ���������)
		randomMas(pmas);//������������ ��������� ������� �������� ������ ��������� (��������� '*')
		randomOthers(pmas);//����������� �������� � ������ ��������� �������
		system("cls");//������� ������
		printAll(pmas);//����� ������� �� ����� (���������)
		start=clock();
		while(clock()-start<delay);//��������� ��������
	}
	return 0;
}