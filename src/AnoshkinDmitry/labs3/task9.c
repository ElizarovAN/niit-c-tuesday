/*
 �������� ���������, ������������ � ������ ���������� ������������������
������������ �����
���������:
��� ������ AABCCCDDEEEEF ��������� 4 � EEEE
*/
// by Anoshkin Dmitry

#include <stdio.h>

#define MaxLen 100

int main()
{
    char str[MaxLen]={0};
    char i=0, j=0;
    char max=0, current=0;
    char MaxIndex=0;
    puts ("Input string: ");
    fgets(&str, MaxLen, stdin);
    while (str[i] && str[i]!=10)
    {
        j=i;
        while (str[i]==str[j++])
            current++;
        if (current>max)
        {
            max=current;
            MaxIndex=i;
        }
        i=i+current;
        current=0;
    }
    printf ("%d - ", max);
    for (i=MaxIndex; i<MaxIndex+max; i++)
        putchar(str[i]);
    putchar('\n');
    return 0;
}
