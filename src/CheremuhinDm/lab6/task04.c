//Написать программу, которая суммирует массив традиционным и
//рекурсивным способами
// При резервируемом стеке 256 МБ, максимальная степень двойки, 
// не вызывающая переполнения - 20.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

typedef unsigned long long ULL;

void Summ(clock_t*p_t, ULL*p_summ, short*dyn_arr, int *N) {
	*p_summ += dyn_arr[*N-1];
	(*N)--;
	if (*N == -1) *p_t = clock() - *p_t;
	if (*N >= 0) Summ(p_t, p_summ, dyn_arr, N);
			
}

int main() {
	int N, M, i;
	ULL summ = 0;
	short*dyn_arr = NULL;
	clock_t t;
	clock_t*p_t;
	p_t = &t;

	srand(time(0));
	N = intPower(2, (M = getNumber(1, 20)));
	dyn_arr = (short*)calloc(N, sizeof(short));
	for(i = 0; i < N - 1; i++) { 
		dyn_arr[i] = rand() % 101;  
	}
	t = clock();
	for(i = 0; i < N; i++) {
		summ += dyn_arr[i];
	}
	t = clock() - t;
	printf("Cycle usage time is: %f\n", (double)t);
	summ = 0;
	t = clock();
	Summ(p_t, &summ, dyn_arr, &N);
	printf("Recursion usage time is: %f\n", (double)t);
return 0;
}