﻿/*Написать программу, которая находит в диапазоне целых чисел от
2 до 1000000 число, формирующее самую длинную последователь-
ность Коллатца*/
#include <stdio.h>
#define M 2//начало
#define N 1000000//конец
typedef unsigned long long ULL;
ULL i=0;
ULL colc(ULL K)
{
	i++;
	if(K==1)
		return 0;
	else{
		if(K%2==0)
			colc(K/2);
		else
			colc(3*K+1);
	}
}
int main()
{
	ULL count,number,max=0;
	for(count=M;count<N;count++)
	{
		colc(count);
		if(i>max)
		{
			max=i;
			number=count;
		}
		i=0;
	}	
	printf("The number is %d\n",number);
	return 0;
}