/*
�������� ��������� ������������, ��������� �� ����� ������-
�����, ������������ �� ����������� ������������� ��������� �*�.
����������� ����������� � ��������� ���������� �������, � ��-
��� ��� ����� � ����������� ���������� � ��������� ��� �����.
���������:
������� ������ ��������� � ���� ��������� ������������������ �����:
1) ������� ������� (���������� ���������)
2) ������������ ��������� ������� �������� ������ ��������� (���������
�*�)
3) ����������� �������� � ������ ��������� �������
4) ������� ������
5) ����� ������� �� ����� (���������)
6) ��������� ��������
7) ������� � ���� 1.
*/
//��������� �����
//�������� ���-�� ����� �� 30*
//������� �� ����������: ��������� ����������� ��������� (������� �������� � ��������� �������)

//by Anoshkin Dmitry

#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define size 30

void clean_stdin()  // ������� ������� ������
{
     int c;
     do
     {
         c=getchar();
     }
     while (c!='\n' && c!=EOF);
}

void ArrClear (char (*arr) [size], int N)
{
    int i, j;
    for (i=0; i<N; i++)
        for (j=0; j<N; j++)
            arr[i][j]=' ';
}

void ArrRand (char (*arr) [size], int N)
{
    int i, j;
    srand(time(0));
    for (i=0; i<=N/2; i++)
        for (j=0; j<=N/2; j++)
            if (rand()%3) arr[i][j]=' '; 
            else arr[i][j]='*';
}

void K_ScopeIT (char (*arr) [size], int N)
{
    int i,j;
    for (i=0; i<=N/2; i++)
        for (j=0; j<=N/2; j++)
        {
            arr[N-2-i][j]=arr[i][j];
            arr[i][N-2-j]=arr[i][j];
            arr[N-2-i][N-2-j]=arr[i][j];
        }
}

int main()
{
    char arr[size-1][size]={0};
    char again='y';
    int i=0;
    int M;
    clock_t now;
    while (again=='y')
    {
        ArrClear (arr, size-1);
        ArrRand (arr, size-1);
        K_ScopeIT (arr, size-1);
        for (i=0;i<size-1;i++)
            printf("%s\n", arr[i]);
        now=clock();
        while(clock()<now+3000);
//        puts("One more time 'y/n'?");
//        again=getchar ();
//        clean_stdin();
        system ("cls");
    }
    return 0;
}