/*
1. �������� ���������, ������� ��������� � ��������� ����������
������� ����������� ����������� � ������� ��� �� ������� (��.
�������)
���������:
��������� ������ �������� �����������, ����� ����� �������� ������� ��-
����. �������, ��� ��������� �������� ���������� �������� � ���������
���������� �����������, ������������ ������ �����������.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MaxSize 250 //������������ ������

int squre(int n, int k)             //���������� � �������
{
    if (k<=0)
        return 1;
    return n*squre(n, k-1);
}

int SolveFrac (char (*stars)[MaxSize], int x0, int y0, int k)
{
    int t=k;

    while (t>0)
        t=SolveFrac(stars, x0-squre(3, k-1), y0, t-1);
    t=k;
    while (t>0)
        t=SolveFrac(stars, x0+squre(3, k-1), y0, t-1);
    t=k;
    while (t>0)
        t=SolveFrac(stars, x0, y0-squre(3, k-1), t-1);
    t=k;
    while (t>0)
        t=SolveFrac(stars, x0, y0+squre(3, k-1), t-1);
    t=k;
    while (t>1)
        t=SolveFrac(stars, x0, y0, t-1);

    stars[x0][y0]='*';

    return t;
}


void fractal(char (*stars)[MaxSize], int k)
{
    int x0, y0, border;
    if(k<0)
        return;
    border=squre(3, k);
    x0=border/2;
    y0=x0;
    k=SolveFrac (stars, x0, y0, k);
    for (x0=0; x0<border; x0++)
        stars[x0][border]=0;
}


int main()
{
    char stars[MaxSize-1][MaxSize]={0};
    int k;
    int i, j, border;

    puts ("Enter a number from 1 to 5");
    scanf ("%d", &k);
    border=squre(3, k);
    if (border<MaxSize)
    {
        for (i=0; i<MaxSize-1; i++)
            for (j=0; j<MaxSize-1; j++)
                stars[i][j]=' ';
        fractal (stars, k);
        for (i=0; i<border; i++)
           printf ("%s\n", stars[i]);
    }
    else
        puts ("k too big");
    return 0;
}
