/*
�������� ���������, ������� ��������� ���� �� ������������ ��-
����� (����, �����) � ����������� (����������). ������ ���-
����� � ���� ���� ����� �����, ��������� � ���� �������������
����� � ��������� �� 1 �����. 1 ��� = 12 ������. 1 ���� = 2.54
��.
*/

// �������� ������� �.�.

#include <stdio.h>
#include <string.h>  // ��������� ������ �� ��������

#define fut 12
#define duim 2.54

void clean_stdin()  // ������� ������� ������
{
     int c;
     do
     {
         c=getchar();
     }
     while (c!='\n' && c!=EOF);
}


int main ()
{
    float rost;             //�������������� ����
    int rostfut, rostduim;  //������� ������
    char cont []={0};
 
    while (1)
    {
        puts("Enter your height '00 00'");
        if (scanf("%d %d", &rostfut, &rostduim)<2 ||
            (rostduim>11 || rostduim<0) ||
            rostfut<0)
        {
            puts ("Input Error!");
            clean_stdin ();
        }
        else
        {
            rost=(rostduim+rostfut*fut)*duim;
            printf("%.1f sm\n", rost);
            break;
        }
    }
return 0;
}
    