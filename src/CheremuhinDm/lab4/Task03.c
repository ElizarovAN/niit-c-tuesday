// Палиндром или нет?
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 100

int main()
{
	int i = 0;
	char text[N] = { 0 };
	char *p_arr = { 0 };
	char *p_arr2 = { 0 };
	// Ввод данных
	puts("Type your text: ");
	fgets(text, N, stdin);	
	p_arr = &text;
	p_arr2 = &text[strlen(text) - 2];
	if (*p_arr == '\n') {
		puts("No words!");
		return 0;
	}	
	// Проверка
	for (i = 0; i < (strlen(*p_arr)-1)/2; i++) {
		if (*(p_arr + i) == *(p_arr2 - i))
			continue;
		else {
			puts("This string is not a palindrome!");
			return 0;
		}
	}
	puts("This string is a palindrome!");
	return 0;
}