// Программа "Калейдоскоп".
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <windows.h>
#include "funct.c"
#define HGT 23
#define WDT 35

void AddSign();
void StrPrnt();
void Spacer();

int main(){
	char stars[HGT][WDT] = {0};
	int i = 0, j = 0;
	while (1){
		system("cls");
		Spacer(&stars, HGT*WDT);
		AddSign(&stars, '*', HGT, WDT);
		StrPrnt(&stars, HGT, WDT);		
		Sleep(2000);
	}
return 0;
}