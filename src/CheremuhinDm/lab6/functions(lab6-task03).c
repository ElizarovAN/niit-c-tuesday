#include <stdio.h>
#include <stdlib.h>
#include "task03.h"

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int Validator(struct BUFFER *buffer) { // Проверка значений в массиве char.
	int i = 0;
	for (i = 0; i < buffer->order; i++) 
		if (buffer->arr[i] - 48 < 0 || buffer->arr[i] - 48 > 9)	return 1;		
 return 0;
}

void Arithm(char*sign, int*index,struct BUFFER *buffer) {
	switch(*sign) {
		case '+': 
			buffer->result = buffer->result + *index;
			break;
		case '-': 
			buffer->result = buffer->result - *index;
			break;
		case '*': 
			buffer->result = buffer->result * *index;
			break;
		case '/': 
			buffer->result = buffer->result / *index;
			break;
		default:
			puts("Error!");
	}
}

void Converter(struct BUFFER *buffer) { // Конвертирует значение char в значение int.
	buffer->result += (buffer->arr[buffer->order - buffer->cnt - 1] - 48)*intPower(10, buffer->cnt);
	buffer->cnt += 1;
	if (buffer->cnt < buffer->order) Converter(buffer);
}

int intPower(int a, int b){ // Возводит нат. число в степень. a - нат. число, b - степень числа
	int i, rez = 1;
	if (b == 0) return 1;
	if (b == 1) return a;
	if (b < 0) { // данную ветку оставляю для возможного переделывания этой функции в тип float и т.п.
		for (i=1; i<=abs(b); i++) { // при отриц. b будет возвращать как при b == 0.
			rez *= a;
		}
		rez = 1/rez;
		return rez;
	} 
	if (b > 1) {
	    for(i = 1; i <= b;) {
	        rez *= a;
			i++;
	    }
	    return rez;
	}
}