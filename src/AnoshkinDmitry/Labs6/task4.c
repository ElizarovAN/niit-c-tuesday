/*
4. �������� ���������, ������� ��������� ������ ������������ �
����������� ���������
���������:
��������� ��������� ��������� ������������������ ��������:
(a) ��������� �� ��������� ������ �������� ������� ������ M;
(b) ������� ������ ������������� ������� N = 2M;
(c) �������� ������ ��� ������������ ������;
(d) ��������� ������� ��������� ������ �������;
(e) ������� ����� ������������ � ������������ ��������;
(f) ���������� ����� ���������� ������������ ������������ � ��������-
��� ��������;
(g) ����������� ������������ ������
*/

//by Anoshkin Dmitry

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#define Min 0
#define Max 10

typedef unsigned char UC;

int multy(int n, int k);    //���������
void RandArr (char*arr, long int M, int min, int max); //randomize arrow from min to max
long int SumArr (char*arr, int M);                      //������������ ������� ������� ��������
long int SumArrR(char*arr, int M);                      //������������ ������� ����������� ��������
long int SumArrCalc (char *arr, long int M, long int pstnl);    //����������� ������������ (���������� �� �������)

int main(int argc, char *argv[])
{
    UC *arr;
    int N;
    long int M, sum;
    clock_t begin, end;
    double delta1=0, delta2=0;
    
    if (argc<1) 
    {
        puts ("Input Error");
        return 1;
    }
    N=atoi(argv[1]);
    if (N<0) 
    {
        puts ("Input Error");
        return 1;
    }
    M=multy(2,N);
    arr=(UC*)malloc(M*sizeof(UC));
    if(!arr)
    {
       puts("Memory allocation error!");
       return 2;
    }
    RandArr(arr, M, Min, Max);
    
    begin=clock();
    sum=SumArr (arr, M);
    end=clock();
    delta1=((double)(end-begin)/CLOCKS_PER_SEC);
    printf ("Usual Summ=%ld is done in %.02lf seconds\n", sum, delta1);
    
    begin=clock();
    sum=SumArrR(arr, M);
    end=clock();
    delta2=((double)(end-begin)/CLOCKS_PER_SEC);
    printf ("Recursive Summ=%ld is done in %.02lf seconds\n", sum, delta2);
    if (delta1<delta2)
        puts ("Usual way is faster");
    else if (delta2<delta1)
        puts ("Recursive way is faster");
    else
        puts("Ways are equal");
    free(arr);
    return 0;
}

int multy(int n, int k)             //���������� � �������
{
    if (k<=0)
        return 1;
    return n*multy(n, k-1);
}

void RandArr (char*arr, long int M, int min, int max)
{
    long int i;
    srand (time(0));
    for(i=0; i<M; i++)
        arr[i]=rand()%(min-max)+min;    
}
long int SumArr (char*arr, int M)
{
    long int i, sum=0;
    for(i=0; i<M; i++)
        sum=sum+arr[i];
    return sum;
}
long int SumArrR(char*arr, int M)
{
    if (M>1)
        return SumArrCalc (arr, M/2, 0)+SumArrCalc(arr, M/2, M/2);
    else 
        return arr[0];
}
long int SumArrCalc (char *arr, long int M, long int pstnl)
{
    if (M<=1)
        return arr[pstnl];
    else
    {
        return SumArrCalc (arr, M/2, pstnl)+SumArrCalc (arr, M/2, pstnl+M/2);
    }
}
