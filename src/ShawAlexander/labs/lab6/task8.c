/*8. �������� ���������, ������� ��������� ������������� ��������� ��������������� ���������, ��������� � ���� ��������� ��������� ������.
������������� ��������� 4-� �������� ��������. ������� ���������� ������������ �������� ��������.
���������:
� ������ ��������� ����� ����������� �������: 0-9,+,-,*,/,(,) ��������� ����� ���� ��������, �� ���� ������� �������� �� ������ ����� 3, 8,
� ����� ���� ��������, ��������, ((6+8)*3) ��� (((7-1)/(4+2))-9) . ��������������, ��� ������ � ��������� ������ ���������,
�� ���� ���������� �������� ����� ���������� �������� � ��� �� ���������� ��������.
��������� ������ �������� �� ��������� �������:
(a) int main(int argc, char* argv[]) - ������� �������, � ������� �������������� ����� ����������� ������� eval ��� ���������� ���������
(b) int eval(char *buf) - �������, ����������� ������, ������������ � buf
(c) char partition(char *buf, char *expr1, char *expr2) - �������, ������� ��������� ������, ������������ � buf �� ��� �����: ������ � ������ ���������, ���� �������� � ������ �� ������ ���������
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define SIZE 128
#define EXP_SIZE (SIZE/2)
int eval(char*);
char partition(char*, char*, char*);
char getOp(char);
int main() {
    char expression[SIZE];
    puts("Enter an expression to evaluate (without spaces):");
    fgets(expression, SIZE, stdin);
    expression[strlen(expression) - 1] = 0;
    printf("Result: %d\n", eval(expression));
    return 0;
}
//����������� ������� ��� ���������� ����������
int eval(char *buf) {
    char exp1[EXP_SIZE] = { 0 };
    char exp2[EXP_SIZE] = { 0 };
    char action = partition(buf, exp1, exp2);
    if (!action)
        return atoi(exp1);
    else {
        if (action == '+')
            return eval(exp1) + eval(exp2);
        else if (action == '-')
            return eval(exp1) - eval(exp2);
        else if (action == '*')
            return eval(exp1) * eval(exp2);
        else if (action == '/')
            return eval(exp1) / eval(exp2);
    }
}
//��������� ������
char partition(char *buf, char *exp1, char *exp2) {
    int brackets = 0;
    int length = strlen(buf);
    char c = 0;
    int i = 0;
    //������� ������ ������
    while (buf[0] == '(' && buf[length - 1] == ')') {
        for (i = 1; i < length - 1; i++) {
            if (brackets < 0) {
                break;
            }
            if (buf[i] == '(')
                brackets++;
            else if (buf[i] == ')')
                brackets--;
        }
        if (!brackets) {
            memcpy(buf, buf + 1, length - 1);
            length--;
            buf[length - 1] = 0;
            length--;
        }
        else {
            brackets = 0;
            break;
        }
    }
    //������� ����� � ������ �����
    for (i = 0; i < length; i++) {
        c = buf[i];
        if (getOp(c) && !brackets) {
            int q = 0;
            //�������� �� ���������� ��������
                char checkChar = 0;
                int checkBrackets = 0;
                for (int s = i; s < length; s++) {
                    checkChar = buf[s];
                    if (checkChar == '(')
                        checkBrackets++;
                    else if (checkChar == ')')
                        checkBrackets--;
                    else if ((checkChar == '+' || checkChar == '-') && checkBrackets <= 0) {
                        c = checkChar;
                        i = s;
                    }
                }
            //������ ����� �����
            for (int k = 0; q < i; q++, k++) {
                exp1[k] = buf[q];
            }
            i++;
            //������ ������ �����
            for (q = 0; i < length; i++, q++) {
                exp2[q] = buf[i];
            }
            //���������� �������������� ������
            return c;
        }
        else if (c == '(') {
            brackets++;
        }
        else if (c == ')')
            brackets--;
    }
    i = 0;
    //���� ������ ������� ������ �� �����, �� ������������� ����� ����� ������ ���� ������
    while (exp1[i++] = *buf++) {}
    return 0;
}
//�������� ������� �� �������������� ����
char getOp(char c) {
    if (c == '+' || c == '-' || c == '*' || c == '/')
        return 1;
    else
        return 0;
}