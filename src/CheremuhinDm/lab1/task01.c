// ��������� ������������� ����������� ����, ����� � ���� ��������.
#define _CRT_SECURE_NO_WARNINGS
#include <string.h>
#include <stdio.h>

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	int wt = 0, gth = 0, sex = 0, optimal_w = 0, h_wt = 150, l_wt = 30, h_gth = 250, l_gth = 120, m_sex = 100, f_sex = 110;
	while (1)
	{
		puts("Write your weight");
		scanf("%d", &wt);
		if (wt >= l_wt && wt <= h_wt)
			break;
		else
		{
			puts("Input error!");
			clean_stdin();
		}
	}
	while (1)
	{
		puts("Write your growth");
		scanf("%d", &gth);
		if (gth >= l_gth && wt <= h_gth)
			break;
		else
		{
			puts("Input error!");
			clean_stdin();
		}
	}
	while (1)
	{
		puts("Write your sex(1 - Man, 2 - Woman");
		scanf("%d", &sex);
		if (sex == 1 || sex == 2)
			break;
		else
		{
			puts("Input error!");
			clean_stdin();
		}
	}
	sex == 1 ? optimal_w = (gth - m_sex) : (optimal_w = (gth - f_sex));
	optimal_w > wt ? puts("Your weight is low!") : (optimal_w < wt ? puts("Your weight is high!") : (puts("Your weight is optimal!")));
	return 0;
}