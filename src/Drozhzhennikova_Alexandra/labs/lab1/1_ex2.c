#include<stdio.h>

void clean_stdin()
{
	int c;
	do{
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	int hours;
	int minutes;
	int seconds;

	while (1)
	{

		printf("Enter hours, minutes and secundes:\n");
		scanf("%d %d %d", &hours, &minutes, &seconds);
		printf("You have entered %d:%d:%d \n", hours, minutes, seconds);

		if ((hours<24 && hours>0) && (minutes<60 && minutes>0) && (seconds<60 && seconds>0))
		{

			if (hours >= 4 && hours <= 11)
				printf("Good morning!\n");
			if (hours > 11 && hours <= 15)
				printf("Good day!\n");
			if (hours > 15 && hours <= 21)
				printf("Good evening!\n");
			if (hours > 21 && hours < 4)
				printf("Good night!\n");
			break;
		}
		else
			printf("Error input!\n");
	
	}
		

	clean_stdin();
	

	return 0;
}