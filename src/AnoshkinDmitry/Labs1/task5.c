/*
�������� ���������, ������� ��������� ������ �� ������������ �
������� � �� �����, ���������� �� ������
*/

// �������� ������� �.�.

#include <stdio.h>
#include <string.h>

#define MaxLen 80
#define ststr '%'
#define endstr 's'

int main()
{
    char Inpstr[MaxLen], Outstr [MaxLen];
    char format[20]={'\0'};
    int l, f;

    while (1)                           // ���� ������
    {
        puts ("Enter your string");
        scanf ("%s", &Inpstr);
        l=strlen (Inpstr);
        if (l>MaxLen)
            puts ("Input Error!");
        else
            break;
    }
    f=MaxLen/2+(int)(l/2);              //��������
    sprintf (format, "%c%d%c\n", ststr, f, endstr); //������� ������ ������
    printf (format, Inpstr);
    return 0;   
}
