/*�������� ���������, ������� �������� ����� ���������� N-���
����� ���� ���������. ������������� ����� ������� ��������
��� N � ��������� �� 1 �� 40 (��� � ������ ��������� �� �������)
�� ����� � � ����
���������:
��������� ���� �� ���������� ����� ������� � ����������� ������� � ��-
������� ������ ����������� ������� �� ����� ���� N*/
#include <stdio.h>
#include <time.h>
#define CLOCKS_PER_SEC 1000
#define COUNT 80
typedef unsigned long long ULL;
ULL fib(ULL K, ULL M, int N)
{
	if(N==1)
		return M;
	else
		return fib(M,K+M,N-1);
}
int main()
{
	FILE *out;
	int i;
	clock_t t;
	out=fopen("result.xls","wt");
	for(i=1;i<=COUNT;i++)
	{
		t=clock();
		fib(0,1,i);
		t=clock()-1;
		printf("%2d - %lld %f seconds\n",i,fib(0,1,i),((double)t)/CLOCKS_PER_SEC);
		fprintf(out,"%2d - %lld %f seconds\n",i,fib(0,1,i),((double)t)/CLOCKS_PER_SEC);
	}
	fclose(out);
	return 0;
}