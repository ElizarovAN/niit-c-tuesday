/*�������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
���������:
��������� ������ �������� ������� �� ���� �������:
a) printWord - ������� ����� �� ������ (�� ����� ������ ��� �������)
b) getWords - ��������� ������ ���������� �������� ������ ���� ����
c) main - �������� �������*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 80//����� ������
int WordCount(char *str, int size)//������� ��� �������� ���� � ������
{
	int i,count=0,inword=0;
	for(i=0;i<size;i++)
	{
		if (*(str+i)==' ' || *(str+i)=='\n' || *(str+i)=='\t')
			inword=0;
		else if (inword==0)
		{
			inword=1;
			count++;
		}	
	}
	return count;
}
void getWord(char *str, char **pstr, int size)//��������� ������ ���������� �������� ������ ���� ����
{
	int i,j=0,inword=0;
	for(i=0;i<size;i++)
	{
		if (*(str+i)==' ' || *(str+i)=='\n' || *(str+i)=='\t')
			inword=0;
		else if (inword==0)
		{
			inword=1;
			pstr[j]=&str[i];
			j++;
		}
	}	
}
void random(char **pstr, int count)//������������ ������ ����������
{
	int i,j=0;
	char *tmp;
	srand(time(NULL));
	for(i=0;i<count;i++)
	{
		j=rand()%(count-i)+i;
		tmp=pstr[j];
		pstr[j]=pstr[i];
		pstr[i]=tmp;
	}
}
void printWord(char *pstr)//������� ����� �� ������ (�� ����� ������ ��� �������)
{
	int i=0;
	while(pstr[i]!=' ' && pstr[i]!='\0' && pstr[i]!='\t' && pstr[i]!='\n')
	{ 
		putchar(pstr[i]);
		i++;
	}
}
int main() 
{
	char str[N],**pstr;
	int i,size,count;
	puts("Enter the line: ");
	fgets(str,N,stdin);
	size=strlen(str);//size ���-�� ��������� � ������
	count=WordCount(str,size);//count ���-�� ���� � ������
	pstr=(char**)malloc(count*sizeof(char*));//������ ����������
	if(pstr == NULL) 
	{
		printf("Error!\n");
		exit(1);
	}
	getWord(str,pstr,size);
	random(pstr,count);
	for(i=0;i<count;i++)
	{
		printWord(pstr[i]);
		printf(" ");
	}
	free(pstr);
	return 0;
}